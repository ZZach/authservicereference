const serverless = require('serverless-http');
const express = require('express');
const nodepath = require('path');
const app = require('./app');
const router = require('./router');
require('./static');
const zoom = require('./zoom');
require('./services/configure-services');

console.log("dirname:",__dirname);
console.log('path: ', `/dev/${process.env.app}`);
router.use(express.static(nodepath.join(__dirname, '../staticBuild')));

app.use(`/${process.env.app}`, router);

const handler = serverless(app);
module.exports.handler = async (event, context) => {
    console.log('Remaining time: ', context.getRemainingTimeInMillis());
    console.log('Function name: ', context.functionName);
    context.callbackWaitsForEmptyEventLoop = false;
    if (event.type && event.type == 'scheduled') {
        await zoom.runSchedule();
    }
    return await handler(event, context);
};
