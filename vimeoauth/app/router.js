const express = require('express');

const router = express.Router();

router.get('/testPageRequest', (req, res) => {
    res.status(200).send('test page request success');
});

module.exports = router;
