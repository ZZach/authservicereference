'use strict';
const aws = require('aws-sdk');
const config = require('../config');

class DynamoDBUtil {
    deauthorize(accountId) {
        // console.log(accountId);
        return new Promise((resolve, reject) => {
            const params = {
                TableName: config.userInfoTable,
                IndexName: 'accountId-index',
                KeyConditionExpression: 'accountId = :aid',
                ExpressionAttributeValues: { ':aid': accountId }
            };
            const docClient = new aws.DynamoDB.DocumentClient({ region: 'us-east-1' });
            docClient.query(params, (err, data) => {
                if (err) {
                    console.log(err);
                    reject(err);
                } else {
                    for (let i = 0; i < data.Items.length; i++) {
                        // console.log(data.Items[i].userId);
                        const deleteParam = {
                            TableName: config.userInfoTable,
                            Key: { accountId, userId: data.Items[i].userId }
                        };
                        docClient.delete(deleteParam, (error, datadeleted) => {
                            if (error) {
                                console.log(error);
                                reject(error);
                            }
                            resolve(true);
                        });
                    }
                }
            });
        });
    }

    async storeZoomToken(accountId, userId, tokens) {
        const checkZoomToken = await this.getZoomAccessToken(accountId);
        if (checkZoomToken && checkZoomToken != null) {
            return new Promise((resolve, reject) => {
                const params = {
                    TableName: config.userInfoTable,
                    Key: {
                        accountId,
                        userId: '1'
                    },
                    UpdateExpression: 'set zoomAccessTokens = :zoomAccessTokens',
                    ExpressionAttributeValues: {
                        ':zoomAccessTokens': tokens
                    },
                    ReturnValues: 'UPDATED_NEW'
                };
                // var dynamodb = new aws.DynamoDB({ region: 'us-east-1' });
                const docClient = new aws.DynamoDB.DocumentClient({ region: 'us-east-1' });
                docClient.update(params, (err, data) => {
                    if (err) {
                        console.log(`storeZoomToken Unable to update tokens. Error JSON:${JSON.stringify(err, null, 2)}`);
                        reject(err);
                    } else {
                        console.log(`storeZoomToken token successfully:${JSON.stringify(data, null, 2)}`);
                        resolve(`storeZoomToken token successfully:${JSON.stringify(data, null, 2)}`);
                    }
                });
            });
        }
        return new Promise((resolve, reject) => {
            const params = {
                TableName: config.userInfoTable,
                Item: {
                    accountId,
                    userId: '1',
                    zoomAccessTokens: tokens,
                    zoomAdminId: userId
                }
            };

            const docClient = new aws.DynamoDB.DocumentClient();
            docClient.put(params, (err, data) => {
                if (err) {
                    console.log('Unable to read item. Error JSON:', JSON.stringify(err, null, 2));
                    reject(err);
                } else {
                    console.log('Added token successfully:', JSON.stringify(data, null, 2));
                    resolve(`Added token successfully:${JSON.stringify(data, null, 2)}`);
                }
            });
        });
    }

    getZoomAccessToken(accountId) {
        return new Promise((resolve, reject) => {
            const params = {
                TableName: config.userInfoTable,
                Key: {
                    accountId,
                    userId: '1'
                }
            };

            const dynamodb = new aws.DynamoDB({ region: 'us-east-1' });
            const docClient = new aws.DynamoDB.DocumentClient({ service: dynamodb });

            docClient.get(params, (err, data) => {
                if (err) {
                    console.log('getZoomAccessToken Unable to read item. Error JSON:', JSON.stringify(err, null, 2));
                    reject(err);
                } else {
                    console.log('Successfully fetched the tokens for clientId: ', config.clientID);
                    // console.log(data);
                    resolve(data.Item);
                }
            });
        });
    }

    storeTpTokens(aID, userID, tokens) {
        return new Promise((resolve, reject) => {
            const params = {
                TableName: config.userInfoTable,
                Item: {
                    accountId: aID,
                    userId: userID,
                    tpTokens: tokens,
                    subscriptionInfo: {}
                }
            };

            const docClient = new aws.DynamoDB.DocumentClient();
            docClient.put(params, (err, data) => {
                if (err) {
                    console.log('Unable to read item. Error JSON:', JSON.stringify(err, null, 2));
                    reject(err);
                } else {
                    console.log('Added token successfully:', JSON.stringify(data, null, 2));
                    resolve(true);
                }
            });
        });
    }

    updateOrdID(aID, userID, tokens) {
        return new Promise((resolve, reject) => {
            const params = {
                TableName: config.userInfoTable,
                Key: {
                    accountId: aID,
                    userId: userID
                },
                UpdateExpression: 'set tpTokens = :tpTokens',
                ExpressionAttributeValues: {
                    ':tpTokens': tokens
                },
                ReturnValues: 'UPDATED_NEW'
            };
            // var dynamodb = new aws.DynamoDB({ region: 'us-east-1' });
            const docClient = new aws.DynamoDB.DocumentClient({ region: 'us-east-1' });
            docClient.update(params, (err, data) => {
                if (err) {
                    console.log(`updateOrdID Unable to update tokens. Error JSON:${JSON.stringify(err, null, 2)}`);
                    reject(err);
                } else {
                    console.log(`updateOrdID token successfully:${JSON.stringify(data, null, 2)}`);
                    resolve(true);
                }
            });
        });
    }

    getTpTokens(aID) {
        return new Promise((resolve, reject) => {
            const params = {
                TableName: config.userInfoTable,
                FilterExpression: 'accountId = :accountId AND userId  <> :userId',
                ExpressionAttributeValues: {
                    ':accountId': aID,
                    ':userId': '1'
                }
            };

            const docClient = new aws.DynamoDB.DocumentClient();
            docClient.scan(params, (err, data) => {
                if (err) {
                    console.log('getTpTokens: Unable to read item. Error JSON:', JSON.stringify(err, null, 2));
                    reject(err);
                } else {
                    console.log('getTpTokens Info:', JSON.stringify(data));
                    if (data.Items && data.Items.length > 0) {
                        console.log('======>', data.Items);
                        resolve(data.Items[0]);
                    } else {
                        console.log('======> False: ', data.Item);
                        resolve([]);
                    }
                }
            });
        });
    }

    getTpTokensUsingAccountidAndUserid(accountId, userId) {
        return new Promise((resolve, reject) => {
            const params = {
                TableName: config.userInfoTable,
                Key: {
                    accountId,
                    userId
                }
            };

            const dynamodb = new aws.DynamoDB({ region: 'us-east-1' });
            const docClient = new aws.DynamoDB.DocumentClient({ service: dynamodb });

            docClient.get(params, (err, data) => {
                if (err) {
                    console.log('getTpTokensUsingAccountidAndUserid Unable to read item. Error JSON:', JSON.stringify(err, null, 2));
                    reject(err);
                } else {
                    console.log('getTpTokensUsingAccountidAndUserid : ', data.Item);
                    resolve(data.Item);
                }
            });
        });
    }


    saveConnectionTokens(accountId, userId, cTokens) {
        return new Promise((resolve, reject) => {
            const params = {
                TableName: config.userInfoTable,
                Key: {
                    accountId,
                    userId
                },
                UpdateExpression: 'set zoomConnectionTokens = :zoomConnectionTokens',
                ExpressionAttributeValues: {
                    ':zoomConnectionTokens': cTokens
                },
                ReturnValues: 'UPDATED_NEW'
            };
            // var dynamodb = new aws.DynamoDB({ region: 'us-east-1' });
            const docClient = new aws.DynamoDB.DocumentClient({ region: 'us-east-1' });
            docClient.update(params, (err, data) => {
                if (err) {
                    console.log('saveConnectionTokens Unable to update tokens. Error JSON:', JSON.stringify(err, null, 2));
                    reject(err);
                } else {
                    console.log('Updated saveConnectionTokens successfully:', JSON.stringify(data));
                    resolve(true);
                }
            });
        });
    }

    disconnectThirdPartyAuth(aID, uID) {
        return new Promise((resolve, reject) => {
            const params = {
                TableName: config.userInfoTable,
                Key: {
                    accountId: aID,
                    userId: uID
                }
            };
            // docClient.delete(deleteParam, function(error, datadeleted){});
            const docClient = new aws.DynamoDB.DocumentClient();
            docClient.delete(params, (err, data) => {
                if (err) {
                    console.log('disconnect3rdParyAuth: Unable to read item. Error JSON:', JSON.stringify(err, null, 2));
                    resolve('false');
                } else {
                    console.log('getTpTokens Info:', JSON.stringify(data));
                    resolve('true');
                }
            });
        });
    }

    getSubscriptionInfo(accountId) {
        return this.getTpTokens(accountId);
    }

    updateSubscriptionInfo(accountId, userId, subscriptionInfo) {
        return new Promise((resolve, reject) => {
            const params = {
                TableName: config.userInfoTable,
                Key: {
                    accountId,
                    userId
                },
                UpdateExpression: 'set subscriptionInfo=:subsubscriptionInfo',
                ExpressionAttributeValues: {
                    ':subsubscriptionInfo': subscriptionInfo
                },
                ReturnValues: 'UPDATED_NEW'
            };

            const dynamodb = new aws.DynamoDB({ region: config.region });
            const docClient = new aws.DynamoDB.DocumentClient({ service: dynamodb });
            docClient.update(params, (err, data) => {
                if (err) {
                    console.log('Unable to update subscription. Error JSON: ', JSON.stringify(err, null, 2));
                    reject(err);
                } else {
                    console.log('Updated subscription details successfully: ', JSON.stringify(data, null, 2));
                    resolve(data);
                }
            });
        });
    }

    updateZoomConnectionTokens(accountId, connectionTokens) {
        return new Promise((resolve, reject) => {
            const params = {
                TableName: config.userInfoTable,
                Key: {
                    accountId,
                    userId: '1'
                },
                UpdateExpression: 'set zoomConnectionTokens=:zoomConnectionTokens',
                ExpressionAttributeValues: {
                    ':zoomConnectionTokens': connectionTokens
                },
                ReturnValues: 'UPDATED_NEW'
            };

            const dynamodb = new aws.DynamoDB({ region: config.region });
            const docClient = new aws.DynamoDB.DocumentClient({ service: dynamodb });
            docClient.update(params, (err, data) => {
                if (err) {
                    console.log('Unable to update subscription. Error JSON: ', JSON.stringify(err, null, 2));
                    reject(err);
                } else {
                    console.log('Updated subscription details successfully: ', JSON.stringify(data, null, 2));
                    resolve(data);
                }
            });
        });
    }

    getSubscriptionInfoForAll() {
        return new Promise((resolve, reject) => {
            const params = {
                TableName: config.userInfoTable
            };
            const dynamodb = new aws.DynamoDB({ region: config.region });
            const docClient = new aws.DynamoDB.DocumentClient({ service: dynamodb });
            docClient.scan(params, (err, data) => {
                if (err) {
                    // console.log("getTpTokens: Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
                    reject('getTpTokens: Unable to read item. Error JSON:', JSON.stringify(err, null, 2));
                } else {
                    // console.log("getTpTokens Info:", JSON.stringify(data));
                    if (data.Items && data.Items.length > 0) {
                        // console.log("======>" + data.Items);
                        resolve(data.Items);
                    } else {
                        // console.log("======> False: " + data.Item);
                        resolve(data.Items);
                    }
                }
            });
        });
    }
}

// async function checkDelete(){
//     try{
//         let response = await new DynamoDBUtil().deauthorize("3dEVoqekQXeW_Wqw0q4guA");
//         console.log(response);
//     }catch( e){
//         console.log(e);
//     }

// }
// checkDelete();
module.exports = DynamoDBUtil;
