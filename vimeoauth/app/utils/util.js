const CryptoJS = require('crypto-js');
const config = require('./../config');

const Util = {

    dateAdd: (date, interval, units) => {
        let ret = new Date(date); // don't change original date
        const checkRollover = () => { if (ret.getDate() != date.getDate()) ret.setDate(0); };
        switch (interval.toLowerCase()) {
        case 'year': ret.setFullYear(ret.getFullYear() + units); checkRollover(); break;
        case 'quarter': ret.setMonth(ret.getMonth() + 3 * units); checkRollover(); break;
        case 'month': ret.setMonth(ret.getMonth() + units); checkRollover(); break;
        case 'week': ret.setDate(ret.getDate() + 7 * units); break;
        case 'day': ret.setDate(ret.getDate() + units); break;
        case 'hour': ret.setTime(ret.getTime() + units * 3600000); break;
        case 'minute': ret.setTime(ret.getTime() + units * 60000); break;
        case 'second': ret.setTime(ret.getTime() + units * 1000); break;
        default: ret = undefined; break;
        }
        return Math.floor(ret.getTime() / 1000);
    },


    dateSubtract: (date, interval, units) => {
        let ret = new Date(date); // don't change original date
        const checkRollover = () => { if (ret.getDate() != date.getDate()) ret.setDate(0); };
        switch (interval.toLowerCase()) {
        case 'year': ret.setFullYear(ret.getFullYear() - units); checkRollover(); break;
        case 'quarter': ret.setMonth(ret.getMonth() - 3 * units); checkRollover(); break;
        case 'month': ret.setMonth(ret.getMonth() - units); checkRollover(); break;
        case 'week': ret.setDate(ret.getDate() - 7 * units); break;
        case 'day': ret.setDate(ret.getDate() - units); break;
        case 'hour': ret.setTime(ret.getTime() - units * 3600000); break;
        case 'minute': ret.setTime(ret.getTime() - units * 60000); break;
        case 'second': ret.setTime(ret.getTime() - units * 1000); break;
        default: ret = undefined; break;
        }
        return Math.floor(ret.getTime() / 1000);
    },

    isEmpty: obj => Object.keys(obj).length === 0,

    getKeyByValue: (object, value) => Object.keys(object).find(key => object[key] === value),

    getValues: object => Object.keys(object).map(k => object[k]),

    encrypt: plainText => CryptoJS.AES.encrypt(plainText, config.encryption_key).toString(),

    decrypt: (cipherText) => {
        const bytes = CryptoJS.AES.decrypt(cipherText, config.encryption_key);
        const plain = bytes.toString(CryptoJS.enc.Utf8);
        return plain;
    },

    encode: (text) => {
        const buff = Buffer.from(text);
        const base64data = buff.toString('base64');
        return base64data;
    },

    decode: (data) => {
        const buff = Buffer.from(data, 'base64');
        const text = buff.toString('ascii');
        return text;
    },

    isIntervalOf15Mins: (currentDate, date2) => {
        const fifteenMinsinMillis = 900000;
        const diff = currentDate - date2;
        if (diff < fifteenMinsinMillis) {
            console.log("new video/project/group has been created");
            return true;
        }
        console.log("nothing found");
        return false;
    }, 
};

module.exports = Util;
