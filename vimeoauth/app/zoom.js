const { oauth2, client } = require('zoom-bot-sdk');
const request = require('request');
const CryptoJS = require('crypto-js');
const router = require('./router');
const config = require('./config');
const DynamoDBUtil = require('./utils/dbutil');
const util = require('./utils/util');
const CommonService = require('./services/common-service');
const aws = require('aws-sdk');
const fetch = require('node-fetch');
const {uuid} = require('uuidv4');
// see https://www.npmjs.com/package/zoom-bot-sdk for detail

const oauth2Client = oauth2(
    config.clientID,
    config.clientSecret,
    config.redirect_uri
);

oauth2Client.on('error', (err) => {
    console.log('_error in oauth2Client');
    console.log(err);
});


// Empty check
const checkEmpty = obj => Object.keys(obj).length === 0;

function GetImAccessToken(accountIdOfUser, userIdOfUser) {
    return new Promise((resolve, reject) => {
        const clientCredentialsURL = `https://zoom.us/oauth/token?grant_type=client_credentials&client_id=${config.clientID}&client_secret=${config.clientSecret}`;
        request.post(clientCredentialsURL, (error, response, body) => {
            if (error) reject(error);
            else {
                const newZoomImAccessToken = body;
                // console.log(`newZoomImAccessToken=====>${newZoomImAccessToken}`);
                const db = new DynamoDBUtil();
                db.storeZoomToken(accountIdOfUser, userIdOfUser, newZoomImAccessToken).then((result) => {
                    const getBody = JSON.parse(newZoomImAccessToken);
                    const newZoomAccessToken = getBody.access_token;
                    resolve(newZoomAccessToken);
                });
            }
        }).auth(config.clientID, config.clientSecret);
    });
}

function postMessage(options) {
    console.log('postMessage start');
    return new Promise((resolve, reject) => {
        request(options, (err, response) => {
            console.log(`===============>${response.statusCode}`);
            // console.log(`Post Message Response: ${JSON.stringify(response.body)}`);
            if (response.statusCode == 401 || response.statusCode == 400) {
                resolve(false);
            } else {
                resolve(true);
            }
            if (err) {
                console.log('error in post message :', err);
            }
        });
    });
}

async function sendMessage(accessToken, toJid, robotJid, accountId, userId, visibleTo, userJid, header, body, numTries) {
    console.log('sendMessage start');
    let token = accessToken;
    const postBody = {
        robotJid, toJid, accountId, userJid, content: { head: header, body }
    };
    /* eslint-disable no-await-in-loop */
    if (visibleTo === true) {
        postBody.visible_to_user = userId;
    }

    for (let i = 0; i < numTries; i++) {
        const options = {
            method: 'POST',
            url: 'https://api.zoom.us/v2/im/chat/messages',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: postBody,
            json: true
        };
        console.log('before postmessage options :');
        // console.log(options);
        try {
            const res = await postMessage(options);
            // console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
            // console.log(res);
            // console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
            if (!res) {
                const newToken = await GetImAccessToken(accountId, userId);
                token = newToken;
            } else {
                break;
            }
        } catch (error) {
            console.log(error);
            break;
        }
    }
    console.log('sendMessage end');
    /* eslint-enable no-await-in-loop */
}

function executeMeet(userId, accessToken) {
    return new Promise((resolve, reject) => {
        const d = new Date();
        const meetTime = d.toUTCString();
        const options = {
            method: 'POST',
            url: `https://api.zoom.us/v2/users/${userId}/meetings`,
            headers: {
                Authorization: `Bearer ${accessToken}`,
                'Content-Type': 'application/json'
            },
            body: {
                topic: 'Zoom Meeting',
                type: '2',
                start_time: meetTime,
                duration: 60,
                settings: {
                    join_before_host: true
                }
            },
            json: true
        };
        request(options, (err, response) => {
            if (err) {
                reject(err);
            }
            resolve(response.body);
        });
    });
}

const zoomBot = client(
    config.clientID,
    config.verifyCode,
    config.botJid,
    process.env.app
).commands([
    { command: '/vim connect:', description: 'Authorize and connect your Vimeo account with Zoom,\nSyntax: connect' },
    { command: '/vim configure:', description: 'Configure the channel,\nSyntax: configure' },
    { command: '/vim disconnect:', description: 'Disconnect the Vimeo account from Zoom,\nSyntax: disconnect' },
    { command: '/vim meet:', description: 'Creates instant Zoom meeting,\nSyntax: meet' },
    { command: '/vim get-videos :', description: 'Get 10 recently uploaded videos,\nSyntax: get-videos' },
    { command: '/vim search-videos :', description: 'Get latest 10 videos based on provided video name,\nSyntax: search-videos <video name>' }
]).defaultAuth(oauth2Client.connect());


zoomBot.on('commands', async (e) => {
    try {
        let reqBody = null;
        let reqHeader = null;
        let visibility = false;
        const db = new DynamoDBUtil();
        const cs = new CommonService();
        const {
            payload, command, data
        } = e;
        const {
            toJid, accountId, userId, userJid
        } = payload;

        const dbTpTokens = await db.getTpTokens(accountId);
        const tokens = await db.getZoomAccessToken(accountId);
        if (command.toLowerCase() === 'connect') {
            visibility = true;
            if (typeof dbTpTokens !== 'undefined' && dbTpTokens.tpTokens && !checkEmpty(dbTpTokens.tpTokens)) {
                reqHeader = {
                    text: 'Vimeo Authorized'
                };
                reqBody = [{
                    type: 'message',
                    text: 'Vimeo is already connected, Try the help Command.'
                }];
            } else {
                // console.log(`Account ID===>${accountId}`);
                // console.log(`Userrrr ID===>${userId}`);
                const cipherAccId = CryptoJS.AES.encrypt(`${accountId}###${userId}###con`, config.encryption_key);
                const cipherAccEncodedId = encodeURIComponent(cipherAccId);
                console.log('UNENCODED STATE', cipherAccId.toString());
                console.log('encoded state', cipherAccEncodedId);
                let state = uuid();
                let url = await cs.generateOauthUrl({appname: 'vimeo', oauthurl: `${config.vimeo_connect_url}?client_id=${config.vimeo_client_id}&scope=${config.vimeo_scope}&response_type=${config.vimeo_response_type}&redirect_uri=${config.vimeo_redirect_url}&state=${state}`, information: {key:'authEncode', value: cipherAccEncodedId}, 
                                                        oauthstate: state, redirect_url : config.vimeo_oauthinfo});
                reqHeader = {
                    text: 'Authorize Your Vimeo Account',
                    sub_head: { text: 'Only a Vimeo admin can authenticate.', style: { color: '#FF8C00' } }
                };
                reqBody = [{
                    type: 'message',
                    text: 'Click here to Authorize',
                    link: url
                }];
            }
        } else if (command.toLowerCase() === 'disconnect') {
            if (typeof dbTpTokens !== 'undefined' && dbTpTokens.tpTokens && !checkEmpty(dbTpTokens.tpTokens)) {
                console.log('In Disconnect: ');
                if (dbTpTokens.userId != null && dbTpTokens.userId === userId) {
                    await db.disconnectThirdPartyAuth(accountId, userId);
                    reqHeader = {
                        text: 'Vimeo Disconnected'
                    };
                    reqBody = [{
                        type: 'message',
                        text: 'Vimeo is disconnected from Zoom.'
                    }];
                } else {
                    reqHeader = {
                        text: 'Not Authorized to Disconnect',
                        sub_head: { text: 'Only a Vimeo admin can use the Disconnect Command.', style: { color: '#FF8C00' } }
                    };
                    reqBody = [{
                        type: 'message',
                        text: 'Not authorized to use this command, ask a Vimeo admin to disconnect the account.'
                    }];
                }
            } else {
                visibility = true;
                const cipherAccId = CryptoJS.AES.encrypt(`${accountId}###${userId}###con`, config.encryption_key);
                const cipherAccEncodedId = encodeURIComponent(cipherAccId);
                reqHeader = {
                    text: 'Authorize your Vimeo account first',
                    sub_head: { text: 'Only the Vimeo admin can authenticate.', style: { color: '#FF8C00' } }
                };
                reqBody = [{
                    type: 'message',
                    text: 'Click here to Authorize',
                    link: `${config.vimeo_connect_url}?client_id=${config.vimeo_client_id}&scope=${config.vimeo_scope}&response_type=${config.vimeo_response_type}&redirect_uri=${config.vimeo_redirect_url}&state=${cipherAccEncodedId}`
                }];
            }
        } else if (command.toLowerCase() === 'configure') {
            visibility = true;
            if (typeof dbTpTokens !== 'undefined' && dbTpTokens.tpTokens && !checkEmpty(dbTpTokens.tpTokens)) {
                if (toJid.toLowerCase().indexOf('conference') <= -1) {
                    await cs.updateUserOneToOneChannelInfo(accountId, userId, toJid);
                }
                const isZoomAdmin = await cs.isUserZoomAdmin(accountId, userId);
                const canConfigure = await cs.canUserConfigureSubscriptionsFromChannel(accountId, userId, toJid);

                if (isZoomAdmin || canConfigure) {
                    const cipherAccId = CryptoJS.AES.encrypt(`${accountId}###${userId}###figure`, config.encryption_key);
                    const cipherAccEncodedId = encodeURIComponent(cipherAccId);
                    let url = await cs.generateConfigUrl({appname: 'vimeo', redirectURL: config.notif_setting_page, information: {key:'configureEncode', value: cipherAccEncodedId}});
                    reqHeader = {
                        text: 'Configure Notifications',
                        sub_head: { text: 'Configure channels to get notifications.', style: { color: '#FF8C00' } }
                    };
                    reqBody = [{
                        type: 'message',
                        text: 'Click here to Configure',
                        link: url
                    }];
                } else {
                    reqHeader = {
                        text: 'Not Authorized',
                        sub_head: { text: 'Not authorized to configure notifications.', style: { color: '#FF8C00' } }
                    };
                    reqBody = [{
                        type: 'message',
                        text: 'Notifications to the channel are already configured by another user.'
                    }];
                }
            } else {
                const cipherAccId = CryptoJS.AES.encrypt(`${accountId}###${userId}###con`, config.encryption_key);
                const cipherAccEncodedId = encodeURIComponent(cipherAccId);
                reqHeader = {
                    text: 'Authorize Your Vimeo Account',
                    sub_head: { text: 'Only the Vimeo admin can authenticate.', style: { color: '#FF8C00' } }
                };
                reqBody = [{
                    type: 'message',
                    text: 'Click here to Authorize',
                    link: `${config.vimeo_connect_url}?client_id=${config.vimeo_client_id}&scope=${config.vimeo_scope}&response_type=${config.vimeo_response_type}&redirect_uri=${config.vimeo_redirect_url}&state=${cipherAccEncodedId}`
                }];
            }
        } else if (command.toLowerCase() === 'meet') {
            const connToken = await cs.getConnectionTokens(accountId);
            const getMeetInfo = await executeMeet(userId, connToken.access_token);

            reqHeader = {
                text: 'Join a Zoom meeting'
            };
            reqBody = [{
                type: 'message',
                text: `Meeting ID: ${getMeetInfo.id}`,
                link: `${getMeetInfo.join_url}`
            }];
        } 
        
        else if (command.toLowerCase() === 'get-videos') {
            if (typeof dbTpTokens !== 'undefined' && dbTpTokens.tpTokens && !checkEmpty(dbTpTokens.tpTokens)) {
                const videoResponse = await getVideosForCommand(dbTpTokens.tpTokens.access_token);
                const obj = JSON.parse(videoResponse);
                reqHeader = {
                    text: 'Recently Uploaded Videos:',
                    sub_head: { text: 'Recently Uploaded 10 videos' }
                };
                const rBody = [];
                if(obj.data !== undefined && !util.isEmpty(obj.data) && obj.data.length > 0){
                for (let i = 0; i < obj.data.length; i++) {
                    const currentItem = obj.data[i];

                    const nameObj = {};
                    nameObj.type = 'message';
                    nameObj.text = `Video Name: ${currentItem.name}\nStatus: ${currentItem.status}\nDuration: ${currentItem.duration} Seconds\nCreated time: ${currentItem.created_time}`;
                    rBody.push(nameObj);

                    const linkObj = {};
                    linkObj.type = 'message';
                    linkObj.text = 'Click here to view';
                    linkObj.link = currentItem.link;
                    rBody.push(linkObj);
                }
            }
                if (rBody.length > 0) {
                    reqBody = rBody;
                } else {
                    const nameObj = {};
                    nameObj.type = 'message';
                    nameObj.text = 'No Video Data Found';
                    rBody.push(nameObj);
                    reqBody = rBody;
                }
            } else {
                const cipherAccId = CryptoJS.AES.encrypt(`${accountId}###${userId}###con`, config.encryption_key);
                const cipherAccEncodedId = encodeURIComponent(cipherAccId);
                reqHeader = {
                    text: 'Authorize Your Vimeo Account',
                    sub_head: { text: 'Only the Vimeo admin can authenticate.', style: { color: '#FF8C00' } }
                };
                reqBody = [{
                    type: 'message',
                    text: 'Click here to Authorize',
                    link: `${config.vimeo_connect_url}?client_id=${config.vimeo_client_id}&scope=${config.vimeo_scope}&response_type=${config.vimeo_response_type}&redirect_uri=${config.vimeo_redirect_url}&state=${cipherAccEncodedId}`
                }];
            }
        } else if (command.toLowerCase() === 'search-videos') {
            if (typeof dbTpTokens !== 'undefined' && dbTpTokens.tpTokens && !checkEmpty(dbTpTokens.tpTokens)) {
                const rBody = [];
                console.log("data %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ");
                console.log(data.length);
                if(data.length > 0){

                    const searchResponse = await searchVideos(dbTpTokens.tpTokens.access_token, data);
                    const obj = JSON.parse(searchResponse);
                    reqHeader = {
                        text: 'Vimeo Search results:'
                    };
                    if(obj.data !== undefined && !util.isEmpty(obj.data) && obj.data.length > 0){
                    for (let i = 0; i < obj.data.length; i++) {
                        const currentItem = obj.data[i];
    
                        const nameObj = {};
                        nameObj.type = 'message';
                        nameObj.text = `Video Name: ${currentItem.name}\nStatus: ${currentItem.status}\nDuration: ${currentItem.duration} Seconds\nCreated time: ${currentItem.created_time}`;
                        rBody.push(nameObj);
    
                        const linkObj = {};
                        linkObj.type = 'message';
                        linkObj.text = 'Click here to view';
                        linkObj.link = currentItem.link;
                        rBody.push(linkObj);
                    }
                }

                }else{
                    reqHeader = {
                        text: 'Vimeo Search results:'
                    };
                    const linkObj = {};
                        linkObj.type = 'message';
                        linkObj.text = 'Please enter video name to search';
                        rBody.push(linkObj);
               
        }
                if (rBody.length > 0) {
                    reqBody = rBody;
                } else {
                    const nameObj = {};
                    nameObj.type = 'message';
                    nameObj.text = 'No Search Data Found';
                    rBody.push(nameObj);
                    reqBody = rBody;
                }



                
            } else {
                // console.log(`Account ID===>${accountId}`);
                // console.log(`Userrrr ID===>${userId}`);
                const cipherAccId = CryptoJS.AES.encrypt(`${accountId}###${userId}###con`, config.encryption_key);
                const cipherAccEncodedId = encodeURIComponent(cipherAccId);
                reqHeader = {
                    text: 'Authorize Your Vimeo Account',
                    sub_head: { text: 'Only the Vimeo admin can authenticate.', style: { color: '#FF8C00' } }
                };
                reqBody = [{
                    type: 'message',
                    text: 'Click here to Authorize',
                    link: `${config.vimeo_connect_url}?client_id=${config.vimeo_client_id}&scope=${config.vimeo_scope}&response_type=${config.vimeo_response_type}&redirect_uri=${config.vimeo_redirect_url}&state=${cipherAccEncodedId}`
                }];
            }
        } 
        
        else {
            reqHeader = {
                text: 'Vimeo Command Unknown'
            };
            reqBody = [{
                type: 'message',
                text: 'I did not understand your command, Please try the help command.'
            }];
        }
        // console.log('clienttoken: ', reqBody);
        // console.log(tokens);
       // if (reqBody != null && reqHeader != null && reqBody.length > 0) {
            console.log("visibility ################ ",visibility);
            //await sendMessage(tokens.zoomAccessTokens.access_token, toJid, config.botJid, accountId, userId, visibility, userJid, reqHeader, reqBody, 3);
            let connection = oauth2Client.connect();
            let zoomApp = zoomBot.create({ auth: connection });
            await zoomApp.sendMessage({to_jid: toJid, account_id: accountId, header: reqHeader, body: reqBody});
        //}
    } catch (err) {
        console.log(err);
    }
});

// The entry point for most notifications (other than auth/deauth) from the bot service:
// Slash commands, plus notifications for the interactive UI elements.
router.all('/message', async (req, res) => {
    console.log("message *********************************");
    try {
        req.body.payload.cmd = req.body.payload.cmd.toLowerCase();
        //const { userJid } = req.body.payload;
        const userJid = req.body.payload.userJid;
        if (req.body.payload.cmd === 'help') {
            const messageBody = [
                { type: 'message', text: 'In a group channel, add /vim in front of each command', style: { bold: 'true' } },
                {
                    type: 'fields',
                    items: [
                        { key: 'connect:', value: 'Authorize and connect your Vimeo account with Zoom' },
                        { key: 'configure:', value: 'Configure notifications to channels. New Videos notifications are triggered only if the video is uploaded from the Vimeo Home screen' },
                        { key: 'disconnect:', value: 'Disconnect the Vimeo account from Zoom' },
                        { key: 'meet:', value: 'Create an instant Zoom meeting' },
                        { key: 'get-videos:', value: 'Get 10 recently uploaded videos' },
                        { key: 'search-videos <video name>:', value: 'Get 10 recent videos based on the video name' }
                    ]
                }
            ];
            const db = new DynamoDBUtil();
            const tokens = await db.getZoomAccessToken(req.body.payload.accountId);
            // console.log(`=============>/message${tokens.zoomAccessTokens}`);
            //await sendMessage(tokens.zoomAccessTokens.access_token, req.body.payload.toJid, config.botJid, req.body.payload.accountId, req.body.payload.userId, true, userJid, { text: 'Hi there - I\'m Vimeo chat app', style: { bold: 'true' } }, messageBody, 3);
            //let connection = await oauth2Client.connectByCode(code);
            let connection = oauth2Client.connect();
            let zoomApp = zoomBot.create({ auth: connection });
            await zoomApp.sendMessage({toJid, accountId, body: messageBody, header: { text: 'Hi there - I\'m Vimeo chat app', style: { bold: 'true' } }});
            res.sendStatus(200);
            return;
        }
    } catch (e) {
        console.log(`Error in /message: ${e}`);
    }

    const { body, headers } = req;
    // console.log(`_URLmessage: event = ${body.event} authorization= ${headers.authorization}`);
    // console.log(`     clientid= ${headers.clientid} user-agent= ${headers['user-agent']}`);

    try {
        console.log('  zoomBot.handle call');
        await zoomBot.handle({ body, headers });
        console.log('  zoomBot.handle return');
    } catch (e) {
        console.log('  zoomBot.handle threw exception');
        console.log(e);
    }
    res.send('ok');
    console.log('  URLmessage complete');
});

// Handling OAuth. Extract the code=xxx query parameter, and get an access token
router.get('/auth', async (req, res) => {
    console.log('_OAuth Start');
    try {
        // If you are running on AWS Lambda, you need to store the
        // tokens in a database, so that the next time your AWS Lambda
        // function is called, it can fetch the tokens, and re-create
        // the connection object, then re-create the zoomBot object.
        // Here, we are not saving the tokens, so we will need to
        // completely re-do the OAuth when we get a Slash command.
        // You can either save the tokens here, or in the
        // oauth2Client.on('tokens', tokens) => {}); callback.
        const db = new DynamoDBUtil();
        const connection = await oauth2Client.connectByCode(req.query.code);
        zoomBot.create({ auth: connection });
        const uInfo = await getUserInfoFromToken(connection.tokens.access_token);
        const userInf = JSON.parse(uInfo);
        // console.log(uInfo);
        // console.log(`========>${userInf.account_id}`);
        await GetImAccessToken(userInf.account_id, userInf.id);
        await db.saveConnectionTokens(userInf.account_id, '1', connection.tokens);
        res.redirect(config.zoom_auth_success);
    } catch (e) {
        res.redirect(config.zoom_auth_fail);
    }
});

function getUserInfoFromToken(fTokens) {
    return new Promise((resolve, reject) => {
        const clientInfoURL = `https://zoom.us/v2/users/me?access_token=${fTokens}`;
        request.get(clientInfoURL, (err, res, body) => {
            if (err) {
                console.log(`getUserInfoFromToken Error:${err}`);
                reject(err);
            } else {
                // console.log(`getUserInfoFromToken Body:${body}`);
                resolve(body);
            }
        });
    });
}

function getUserInfoFromUserId(fTokens, userID) {
    return new Promise((resolve, reject) => {
        const options = {
            method: 'GET',
            url: `https://api.zoom.us/v2/users/${userID}`,
            headers: {
                Authorization: `Bearer ${fTokens}`,
                'Content-Type': 'application/json'
            }
        };
        // console.log(`URL${options}`);
        request(options, (err, res, body) => {
            if (err) {
                console.log(`getUserInfoFromUserId Error:${err}`);
                reject(err);
            } else {
                // console.log(`getUserInfoFromUserId Body:${body}`);
                resolve(body);
            }
        });
    });
}

router.post('/deauth', async (req, res) => {
    // req.body = req.body.toString();
    // req.body = JSON.parse(req.body);
    // console.log(req.body.payload);
    try {
        res.setHeader('Strict-Transport-Security', 'max-age=31536000');
        if (req.headers.clientid !== config.clientID
          || req.headers.authorization !== config.verifyCode) {
            console.log('10 Deauth failed validation');
            res.redirect(config.deauth_fail);
        }
        console.log('Performing deauth');
        const db = new DynamoDBUtil();
        const cs = new CommonService();
        // console.log(accountId = ${req.body.payload.account_id.toString()});
        await db.deauthorize(req.body.payload.account_id.toString());
        const checkCompliance = await notifyZoomOfDataCompliance(req, config.clientID, config.clientSecret);
        // console.log('notifyZoomOfDataCompliance Execution Success: ', checkCompliance);
        if (checkCompliance) {
            console.log('Deauth Success!!!');
            res.redirect(config.deauth_success);
        } else {
            console.log('Deauth FAIL, Data Compliance!!!');
            res.redirect(config.deauth_fail);
        }
    } catch (error) {
        console.log(error);
        res.redirect(config.deauth_fail);
    }
 });
 async function notifyZoomOfDataCompliance(req, clientID, clientSecret) {
    return new Promise((resolve, reject) => {
        const options = {
            method: 'POST',
            url: 'https://api.zoom.us/oauth/data/compliance',
            headers: {
                'Content-Type': 'application/json',
                'cache-control': 'no-cache'
            },
            auth: {
                user: clientID,
                pass: clientSecret
            },
            body: {
                client_id: req.headers.clientid,
                user_id: req.body.payload.user_id.toString(),
                account_id: req.body.payload.account_id.toString(),
                deauthorization_event_received: {
                    user_data_retention: 'false',
                    account_id: req.body.payload.account_id.toString(),
                    user_id: req.body.payload.user_id.toString(),
                    signature: req.body.payload.signature.toString(),
                    deauthorization_time: req.body.payload.deauthorization_time.toString(),
                    client_id: req.headers.clientid
                },
                compliance_completed: true
            },
            json: true
        };
        // console.log(options);
        request(options, (err, res, body) => {
            if (err) {
                
                reject(false);
            } else {
                // console.log('notifyZoomOfDataCompliance: ', body);
                console.log(res.statusCode);
                if (res.statusCode == 200) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            }
        });
    });
 }
 
 
// Handling OAuth. Extract the code=xxx query parameter, and get an access token
router.get('/connect', async (req, res) => {
    try {
        console.log('running connect now: state: ', req.query.state);
        console.log('running connect now: code: ', req.query.code);
        console.log('header', req.headers);

        let validatesessionUrl = `https://udnkid8j5e.execute-api.us-east-1.amazonaws.com/dev/authservice/validatesessionandstate?state=${req.query.state}&appname=vimeo`;
        let validated = await fetch(validatesessionUrl, {headers: new fetch.Headers({'Cookie': req.headers.cookie})});
        console.log('validated:', validated);
        let validatedJson=await validated.json();
        console.log('validatedJson ', validatedJson);
        if (!validatedJson.validated) 
        {
            res.send(401);
            return;
        }
        // If you are running on AWS Lambda, you need to store the
        // tokens in a database, so that the next time your AWS Lambda
        // function is called, it can fetch the tokens, and re-create
        // the connection object, then re-create the zoomBot object.
        // Here, we are not saving the tokens, so we will need to
        // completely re-do the OAuth when we get a Slash command.
        // You can either save the tokens here, or in the
        // oauth2Client.on('tokens', tokens) => {}); callback.
        const db = new DynamoDBUtil();
        const cs = new CommonService();

        const stateVal = decodeURIComponent(validatedJson.session.information.authEncode);
        //const stateVal = req.query.state;
        const cipherbytes = CryptoJS.AES.decrypt(stateVal, config.encryption_key);
        const plainBoth = cipherbytes.toString(CryptoJS.enc.Utf8);
        const splitBoth = plainBoth.split('###');
        // console.log(`ACC ID ========>${splitBoth[0]} AND User ID ======> ${splitBoth[1]}`);
        // console.log(`${req.query.code}`);
        if (splitBoth.length < 3 || splitBoth[2] !== "con") {
            console.log("Security check");
            res.redirect(config.vimeo_auth_fail);
            return;
        }
        if (typeof splitBoth !== 'undefined' && splitBoth.length > 1) {
            const response = await fetchAccessToken(req.query.code);
            console.log('fetch access token response', response);
            // console.log(`Response======>${response}`);
            const resObj = JSON.parse(response);
            // console.log(`Response Ojb======>${resObj}`);
            const accessToken = resObj.access_token;
            // console.log(`ACCESS TOKEN======>${accessToken}`);
            if (accessToken && typeof accessToken !== 'undefined') {
                const accessData = {
                    access_token: resObj.access_token,
                    token_type: resObj.token_type,
                    scope: resObj.scope
                };
                const isFinished = await db.storeTpTokens(splitBoth[0], splitBoth[1], accessData);
                // console.log('Updated UserInfo: ', JSON.stringify(response));
                if (isFinished) {
                    console.log('IsFinished True');
                    const zAccessToken = await db.getZoomAccessToken(splitBoth[0]);
                    const connToken = await cs.getConnectionTokens(splitBoth[0]);
                    const getUinfo = await getUserInfoFromUserId(connToken.access_token, splitBoth[1]);
                    const getUinfoObj = JSON.parse(getUinfo);

                    const reqHeader = {
                        text: 'Vimeo Connected'
                    };
                    const reqBody = [{
                        type: 'message',
                        text: 'Vimeo account successfully connected with Zoom'
                    }];
                    await sendMessage(zAccessToken.zoomAccessTokens.access_token, getUinfoObj.jid, config.botJid, splitBoth[0], splitBoth[1], false, getUinfoObj.jid, reqHeader, reqBody, 3);
                    //await zoomBot.sendMessage({getUinfoObj.jid, accountId, body: reqBody, header: reqHeader});
                    res.redirect(config.vimeo_auth_success);
                } else {
                    console.log('IsFinished False');
                    res.redirect(config.vimeo_auth_fail);
                }
            } else {
                console.log('token undefined');
                res.redirect(config.vimeo_auth_fail);
            }
        } else {
            console.log('split failed');
            res.redirect(config.vimeo_auth_fail);
        }
    } catch (e) {
        console.log(e);
        res.redirect(config.vimeo_auth_fail);
    }
});

function fetchAccessToken(code) {
    return new Promise((resolve, reject) => {
        const form = {
            client_id: config.vimeo_client_id,
            client_secret: config.vimeo_client_secret,
            code,
            redirect_uri: config.vimeo_redirect_url,
            grant_type: 'authorization_code'
        };
        // Set the headers
        const headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        };
        const options = {
            url: config.vimeo_access_token_url,
            method: 'POST',
            headers,
            form
        };
        request(options, (err, res, body) => {
            if (err) {
                console.log(err);
                reject(err);
            }
            resolve(body);
        });
    });
}

router.all('/webhook', async (req, res) => {
   // processPolling();
    res.sendStatus(200);
});

router.get('/oauth2', async (req, res) => {
    res.send('hello,this is process.env.app');
});

router.all('/', async (req, res) => {
    console.log('I\'m Vimeo Chat App.');
    res.sendStatus(200);
});

function getVideosForCommand(tpAccessToken) {
    // console.log('getVideosForCommand tpAccessToken',tpAccessToken);
    return new Promise((resolve, reject) => {
        const options = {
            url: 'https://api.vimeo.com/me/videos?direction=desc&per_page=10&sort=date',
            method: 'GET',
            headers: { Authorization: `bearer ${tpAccessToken}` }
        };
        request(options, (err, res, body) => {
            if (err) {
                console.log(err);
                reject(err);
            }
            console.log('get videos response **************************************');
            // console.log(body);
            resolve(body);
        });
    });
}

function searchVideos(tpAccessToken, queryParam) {
    return new Promise((resolve, reject) => {
        const options = {
            url: `https://api.vimeo.com/me/videos?direction=desc&per_page=10&query=${queryParam}&sort=date`,
            method: 'GET',
            headers: { Authorization: `bearer ${tpAccessToken}` }
        };
        request(options, (err, res, body) => {
            if (err) {
                console.log(err);
                reject(err);
            }
            resolve(body);
        });
    });
}

async function processPolling() {
    console.log('processPolling start');
    const db = new DynamoDBUtil();
    const userInfos = await db.getSubscriptionInfoForAll();
    const subscriptionInfo = {};
    for (let i = 0; i < userInfos.length; i++) {
        const userInfo = userInfos[i];

        const { accountId } = userInfo;
        if (!subscriptionInfo[accountId]) {
            subscriptionInfo[accountId] = {};
        }
        if (userInfo.userId == '1') {
            const { zoomConnectionTokens } = userInfo;
            const { zoomAccessTokens } = userInfo;
            subscriptionInfo[accountId].connectionTokens = zoomConnectionTokens;
            subscriptionInfo[accountId].zoomAccessTokens = zoomAccessTokens;
            subscriptionInfo[accountId].zoomAdminId = userInfo.zoomAdminId;
        } else {
            subscriptionInfo[accountId].subscriptionInfo = userInfo.subscriptionInfo;
            subscriptionInfo[accountId].tpTokens = userInfo.tpTokens;
        }
    }
    await processNotifications(subscriptionInfo);
    console.log('processPolling end');
}
async function processNotifications(notifications) {
    // console.log('processNotifications start');
    const accountIds = Object.keys(notifications);
    for (let i = 0; i < accountIds.length; i++) {
        const accountId = accountIds[i];
        const account = notifications[accountId];
        await sendNotificationForAccount(accountId, account);
    }
    // console.log('processNotifications end');
}

async function sendNotification(msg, content, account, accountId, eventType) {
    console.log('sendNotification start');
    let reqHeader = null;
    if (content == 'channel') {
        reqHeader = {
            text: `New ${content} has been subscribed`
        };
    } else {
        reqHeader = {
            text: `New ${content} has been created`
        };
    }
    const reqBody = msg;

    const { subscriptionInfo } = account;
    for (let i = 0; i < Object.keys(subscriptionInfo).length; i++) {
        const subKey = Object.keys(subscriptionInfo)[i];

        const value = subscriptionInfo[subKey];

        for (let j = 0; j < Object.keys(value).length; j++) {
            const key = Object.keys(value)[j];

            const notifArray = value[key].topics;

            const accessToken = JSON.parse(account.zoomAccessTokens).access_token;
            if (notifArray.includes(eventType)) {
                await sendMessage(accessToken, key, config.botJid, accountId, subKey, false, `${subKey.toLowerCase()}@xmpp.zoom.us`, reqHeader, reqBody, 3);
            }
            console.log('sendNotification end');
        }
    }
}

async function sendNotificationForAccount(accountId, account) {
    console.log('sendNotificationForAccount start');
    const timeNow = new Date();
    try {
        const db = new DynamoDBUtil();
        let msg = '';
        const userInfo = await db.getSubscriptionInfo(accountId);


        if (userInfo && userInfo.subscriptionInfo && !util.isEmpty(userInfo.subscriptionInfo)) {
            const projectList = await getProjects(account.tpTokens.access_token,timeNow);
            console.log("New projects created : "+projectList);
            if (!util.isEmpty(projectList) && projectList.items.length > 0) {
                
                const projectDetails = projectList.items;

                for (let i = 0; i < projectDetails.length; i++) {
                    const createdTime = projectDetails[i].created_time;
                    const projectName = projectDetails[i].name;
                    const linkArr = projectDetails[i].uri.split('/');
                    const link = `https://vimeo.com/manage/folders/${linkArr[linkArr.length - 1]}`;

                    const rBody = [];

                    msg = `Project Name: ${projectName}\nCreated on: ${createdTime}`;

                    const nameObj = {};
                    nameObj.type = 'message';
                    nameObj.text = msg;
                    rBody.push(nameObj);

                    const linkObj = {};
                    linkObj.type = 'message';
                    linkObj.text = 'Click here to view';
                    linkObj.link = link;
                    rBody.push(linkObj);
                    if (rBody != '' && !util.isEmpty(rBody)) {
                        await sendNotification(rBody, 'project', account, accountId, 'New Projects');
                    }
                }
            }
            const groupList = await getGroups(account.tpTokens.access_token,timeNow);
            if (!util.isEmpty(groupList) && groupList.items.length > 0) {
                const groupDetails = groupList.items;
                for (let i = 0; i < groupDetails.length; i++) {
                    const createdTime = groupDetails[i].created_time;
                    const groupName = groupDetails[i].name;
                    const groupDesc = groupDetails[i].description;
                    const { link } = groupDetails[i];

                    const rBody = [];

                    msg = `Group Name: ${groupName}\nDescription: ${groupDesc}\nCreated on: ${createdTime}`;

                    const nameObj = {};
                    nameObj.type = 'message';
                    nameObj.text = msg;
                    rBody.push(nameObj);

                    const linkObj = {};
                    linkObj.type = 'message';
                    linkObj.text = 'Click here to view';
                    linkObj.link = link;
                    rBody.push(linkObj);
                    if (rBody != '' && !util.isEmpty(rBody)) {
                        await sendNotification(rBody, 'group', account, accountId, 'New Groups');
                    }
                }
            }
            const videoList = await getVideos(account.tpTokens.access_token, timeNow);
            if (!util.isEmpty(videoList) && videoList.items.length > 0) {
                const videoDetails = videoList.items;
                for (let i = 0; i < videoDetails.length; i++) {
                    const createdTime = videoDetails[i].created_time;
                    const videoName = videoDetails[i].name;
                    const { status } = videoDetails[i];
                    const { duration } = videoDetails[i];
                    const { link } = videoDetails[i];
                    const rBody = [];

                    msg = `Video Name: ${videoName}\nStatus: ${status}\nDuration: ${duration} Seconds` + `\nUploaded on: ${createdTime}`;

                    const nameObj = {};
                    nameObj.type = 'message';
                    nameObj.text = msg;
                    rBody.push(nameObj);

                    const linkObj = {};
                    linkObj.type = 'message';
                    linkObj.text = 'Click here to view';
                    linkObj.link = link;
                    rBody.push(linkObj);
                    if (rBody != '' && !util.isEmpty(rBody)) {
                        await sendNotification(rBody, 'video', account, accountId, 'New Videos');
                    }
                }
            }
        }
    } catch (error) {
        console.log(error);
    }
    console.log('sendNotificationForAccount end');
}

async function getProjects(accessToken,timeNow) {
    return new Promise((resolve, reject) => {
        const options = {
            url: 'https://api.vimeo.com/me/projects',
            method: 'GET',
            headers: { Authorization: `bearer ${accessToken}`, 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36' }
        };

        request(options, (err, res, body) => {
            if (err) {
                console.log(err);
                reject(err);
            }

            const content = JSON.parse(body).data;

            const items = [];
            if (content !== undefined && content.length > 0 && !util.isEmpty(content)) {
                content.forEach((key) => {
                    const createdDate = new Date(key.created_time);
                  
                    if (util.isIntervalOf15Mins(timeNow,createdDate)) {
                        items.push(key);
                    }
                });
            }
            const projectDetails = {

                items
            };
            // console.log(projectDetails);
            resolve(projectDetails);
        });
    });
}

async function getGroups(accessToken,timeNow) {
    return new Promise((resolve, reject) => {
        const options = {
            url: 'https://api.vimeo.com/me/groups',
            method: 'GET',
            headers: { Authorization: `bearer ${accessToken}`, 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36' }
        };
        request(options, (err, res, body) => {
            if (err) {
                console.log(err);
                reject(err);
            }

            const content = JSON.parse(body).data;

            const items = [];
            if (content !== undefined && !util.isEmpty(content) && content.length > 0) {
                content.forEach((key) => {
                   
                    const createdDate = new Date(key.created_time);
                  
                    if (util.isIntervalOf15Mins(timeNow,createdDate)) {
                        items.push(key);
                    }
                    
                });
            }
            const groupDetails = {

                items
            };

            resolve(groupDetails);
        });
    });
}

async function getVideos(accessToken, timeNow) {
    // console.log('getVideos accessToken',accessToken);
    return new Promise((resolve, reject) => {
        const options = {
            url: 'https://api.vimeo.com/me/videos',
            method: 'GET',
            headers: { Authorization: `bearer ${accessToken}`, 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36' }
        };
        request(options, (err, res, body) => {
            if (err) {
                console.log(err);
                reject(err);
            }

            const content = JSON.parse(body).data;
            const items = [];
            if (content !== undefined && !util.isEmpty(content) && content.length > 0) {
                content.forEach((key) => {
                    
                    const createdDate = new Date(key.created_time);
                    
                    if (util.isIntervalOf15Mins(timeNow,createdDate)) {
                        items.push(key);
                        console.log("video details *****************");
                        // console.log(items);
                    }
                });
            }
            const videoDetails = {
                items
            };

            resolve(videoDetails);
        });
    });
}

router.get('/sendRequestToAuth', async (req, res) => {
    res.send(req.cookies);
});

module.exports = {
    runSchedule: async () => {
        // console.log('In Run schedule');
        await processPolling();
    }
};
