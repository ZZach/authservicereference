const router = require('./router.js');

let env = 'production';// default env
if (process.env.NODE_ENV === 'development') {
    env = 'development';
}

if ([true, 'true'].indexOf(process.env.IS_OFFLINE) !== -1) {
    env = 'local';
}


router.get('/page', (req, res) => {
    res.render('index', { app: process.env.app, env, staticPort: process.env.staticPort});
});
