'use strict';
const bodyParser = require('body-parser');
const router = require('../router');
const CommonService = require('./common-service');
const CryptoJS = require('crypto-js');
const config = require('../config');
const fetch = require('node-fetch');
// const notificationTopics = ['Customer Subscription', 'New Customer', 'Application Fee', 'Transfer', 'Card', 'Charge'];
const notificationTopics = ['New Videos', 'New Groups', 'New Projects'];

function decryptAccountUserId(accountUserId){
    let accAndUserId = decodeURIComponent(accountUserId);
    const cipherbytes = CryptoJS.AES.decrypt(accAndUserId, config.encryption_key);
    const plainBoth = cipherbytes.toString(CryptoJS.enc.Utf8);
    const splitBoth = plainBoth.split('###');
    return splitBoth;
}
 

router.post('/subscribedChannels', bodyParser.json(), async (req, res) => {
    // Fetch all the channels the the user has subscribed notifications from
    console.log(' /subscribedChannels ', req.body);
    // we will forward the user's request to auth service
    console.log('cookies', req.cookies.chatbotsession);
    let sessionResult = await fetch(config.getsessioninfoUrl, {headers: new fetch.Headers({'Cookie': req.headers.cookie})});
    let sessionResultJson = await sessionResult.json();
    console.log('sessionResult:', sessionResultJson);
    const mixId  = sessionResultJson.information.configureEncode;
    console.log('mixId', mixId);
    const accountAndUserId = decryptAccountUserId(mixId);
    //const { accountId } = req.body;
    let subscribedChannels = await new CommonService().getSubscriptionInfo(accountAndUserId[0], accountAndUserId[1]);
    console.log('Check info', subscribedChannels);
    if (!subscribedChannels) {
        subscribedChannels = {};
    }
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(subscribedChannels, null, 3));
});

router.post('/allTopics', bodyParser.json(), async (req, res) => {
    // Fetch all the channels the the user has subscribed notifications from
    console.log(' /allTopics ', req.body);
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(notificationTopics, null, 3));
});

router.post('/allChannels', bodyParser.json(), async (req, res) => {
    // Fetch all the channels the the user has subscribed notifications from
    console.log(' /allChannels ', req.body);
    console.log('all channels headers', req.headers.cookie);
    let sessionResult = await fetch(config.getsessioninfoUrl, {headers: new fetch.Headers({'Cookie': req.headers.cookie})});
    let sessionResultJson = await sessionResult.json();
    console.log('allchannels sessionresult ', sessionResultJson);
    const mixId = sessionResultJson.information.configureEncode;
    console.log('getallchannels mixid:', mixId);
   // const { accountId } = req.body;
   const accountAndUserId =  decryptAccountUserId(mixId);
   console.log('getallchannels account id: ', accountAndUserId);
    let channels = await new CommonService().getAllChannels(accountAndUserId[0], accountAndUserId[1]);
    console.log(channels);
    if (!channels || channels.length <= 0) {
        channels = [];
    }
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(channels, null, 3));
});

router.post('/deleteSubscription', bodyParser.json(), async (req, res) => {
    // Fetch all the channels the the user has subscribed notifications from
    console.log(' /saveSubscription ', req.body);
    let validated = await fetch(config.validateUrl+'&csrf='+req.body.csrf, {headers: new fetch.Headers({'Cookie': req.headers.cookie})});
    let validatedJson=await validated.json();
    if (!validatedJson.validated) res.send(401);
    if (!validatedJson.validated) 
    {
        res.send(401);
        return;
    }
    let sessionResult = await fetch(config.getsessioninfoUrl, {headers: new fetch.Headers({'Cookie': req.headers.cookie})});
    let sessionResultJson = await sessionResult.json();
    console.log('allchannels sessionresult ', sessionResultJson);
    const mixId = sessionResultJson.information.configureEncode;
    const accountAndUserId =  decryptAccountUserId(mixId);
    // const { userId } = req.body;
    const channelName = req.body.channel;
    await new CommonService().deleteUserSubscription(accountAndUserId[0], accountAndUserId[1], channelName);
    res.sendStatus(200);
});


router.post('/saveSubscription', bodyParser.json(), async (req, res) => {
    // Fetch all the channels the the user has subscribed notifications from
    console.log(' /saveSubscription ', req.body);
    console.log(req.body);
    let validated = await fetch(config.validateUrl+'&csrf='+req.body.csrf, {headers: new fetch.Headers({'Cookie': req.headers.cookie})});
    console.log('validated:', validated);
    let validatedJson=await validated.json();
    if (!validatedJson.validated) 
    {
        res.send(401);
        return;
    }
    let sessionResult = await fetch(config.getsessioninfoUrl, {headers: new fetch.Headers({'Cookie': req.headers.cookie})});
    let sessionResultJson = await sessionResult.json();
    console.log('allchannels sessionresult ', sessionResultJson);
    const mixId = sessionResultJson.information.configureEncode;
    const accountAndUserId =  decryptAccountUserId(mixId);
    // const { userId } = req.body;
    const channelName = req.body.channel;
    const { topics } = req.body;
    await new CommonService().saveUserSubscription(accountAndUserId[0], accountAndUserId[1], channelName, topics);
    res.sendStatus(200);
});
