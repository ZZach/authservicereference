'use strict';
const { oauth2 } = require('zoom-bot-sdk');
const request = require('request');
const DynamoDBUtil = require('../utils/dbutil');
const config = require('../config');
const utils = require('./../utils/util');
const fetch = require('node-fetch');
const oauth2Client = oauth2(
    config.clientID,
    config.clientSecret,
    config.redirect_uri
);

class CommonService {

    async generateConfigUrl(info)
    {
        console.log('generating url');
        let url = 'https://udnkid8j5e.execute-api.us-east-1.amazonaws.com/dev/authservice/GenerateConfigURL';
        let result = await fetch(url, { method: 'POST', body: JSON.stringify(info), headers: { 'Content-Type': 'application/json' } });
        let resultjson = await result.json();
        console.log('resultjson', resultjson);
        console.log('resultURL: ', resultjson.resultUrl);
        return resultjson.resultUrl;
    }

    async generateOauthUrl(info)
    {
        console.log('generating url');
        console.log(info);
        let url = 'https://udnkid8j5e.execute-api.us-east-1.amazonaws.com/dev/authservice/GenerateOauthURL';
        let result = await fetch(url, { method: 'POST', body: JSON.stringify(info), headers: { 'Content-Type': 'application/json' } });
        let resultjson = await result.json();
        console.log('resultjson', resultjson);
        console.log('resultURL: ', resultjson.resultUrl);
        return resultjson.resultUrl;
    }

    async canUserConfigureSubscriptionsFromChannel(accountId, userId, channelId) {
        console.log('Can User Configure - Start ', userId);
        const db = new DynamoDBUtil();
        try {
            const userInfo = await db.getSubscriptionInfo(accountId);
            if (userInfo && userInfo.subscriptionInfo && !utils.isEmpty(userInfo.subscriptionInfo)) {
                const validChannels = await this.getValidSubscriptionChannelList(accountId, userId);
                if (channelId in validChannels) {
                    console.log('User can configure subscriptions from the channel: ', channelId);
                    return true;
                } if (channelId.indexOf('conference') <= -1) {
                    console.log('User can configure subscriptions from the channel: ', channelId);
                    return true;
                }
            }
        } catch (error) {
            console.log(error);
        }
        console.log('User cannot configure subscriptions from the channel: ', channelId);
        return false;
    }

    async isUserZoomAdmin(accountId, userId) {
        console.log('Is user zoom admin - Start');
        const db = new DynamoDBUtil();
        try {
            const userInfo = await db.getZoomAccessToken(accountId);
            if (userInfo && !utils.isEmpty(userInfo)) {
                const { zoomAdminId } = userInfo;
                if (userId == zoomAdminId) {
                    return true;
                }
            }
        } catch (error) {
            console.log(error);
        }
        return false;
    }

    async isUserTpAdmin(accountId, userId) {
        console.log('Can User Configure - Start ', userId);
        const db = new DynamoDBUtil();
        try {
            const userInfo = await db.getTpTokens(accountId);
            if (userInfo && !utils.isEmpty(userInfo)) {
                const tpUserId = userInfo.userId;
                return tpUserId == userId;
            }
            return true;
        } catch (error) {
            console.log(error);
        }
        console.log(`User ${userId} is not the TP Admin`);
        return false;
    }

    async isAlreadyConnected(accountId) {
        console.log('isAlreadyConnected - Start ', accountId);
        const db = new DynamoDBUtil();
        try {
            const userInfo = await db.getTpTokens(accountId);
            if (userInfo && !utils.isEmpty(userInfo) && userInfo.tpTokens) {
                return true;
            }
            return false;
        } catch (error) {
            console.log(error);
        }
        console.log(`Account ${accountId} is not connected`);
        return false;
    }

    async saveUserSubscription(accountId, userId, channelName, topics) {
        console.log('saveUserSubscription start');
        const db = new DynamoDBUtil();
        let found = false;
        try {
            const isZoomAdmin = await this.isUserZoomAdmin(accountId, userId);
            const isOnetoOne = channelName.indexOf('@') >= 0;
            let othersChannel = false;
            const validChannels = await this.getValidSubscriptionChannelList(accountId, userId);
            if ((validChannels && !utils.isEmpty(validChannels)) || isOnetoOne) {
                console.log('----------------- Valid channels: ', isOnetoOne);
                let channelId = utils.getKeyByValue(validChannels, channelName);
                if (!channelId && isZoomAdmin) {
                    othersChannel = true;
                }
                if (isOnetoOne && !othersChannel) {
                    channelId = `${userId.toLowerCase()}@xmpp.zoom.us`;
                }
                if (channelId && !othersChannel) {
                    const userInfo = await db.getSubscriptionInfo(accountId);
                    if (userInfo && userInfo.subscriptionInfo) {
                        const { subscriptionInfo } = userInfo;
                        if (subscriptionInfo[userId]) {
                            if (subscriptionInfo[userId][channelId]) {
                                subscriptionInfo[userId][channelId].channelName = channelName;
                                subscriptionInfo[userId][channelId].topics = topics;
                            } else {
                                subscriptionInfo[userId][channelId] = {};
                                subscriptionInfo[userId][channelId].channelName = channelName;
                                subscriptionInfo[userId][channelId].topics = topics;
                            }
                        } else {
                            subscriptionInfo[userId] = {};
                            subscriptionInfo[userId][channelId] = {};
                            subscriptionInfo[userId][channelId].channelName = channelName;
                            subscriptionInfo[userId][channelId].topics = topics;
                        }
                        console.log('***** Sub info: ', subscriptionInfo);
                        await db.updateSubscriptionInfo(userInfo.accountId, userInfo.userId, subscriptionInfo);
                        found = true;
                    } else {
                        console.log('No subscription info found');
                    }
                } else {
                    console.log(`Channel ${channelName} is not a valid channel!`);
                }
            }

            if (isZoomAdmin && !found) {
                console.log('');
                const userInfo = await db.getSubscriptionInfo(accountId);
                if (userInfo && userInfo.subscriptionInfo) {
                    const { subscriptionInfo } = userInfo;
                    Object.keys(subscriptionInfo).forEach((usrId) => {
                        Object.keys(subscriptionInfo[usrId]).forEach((channelId) => {
                            const channel = subscriptionInfo[usrId][channelId];
                            if (channel.channelName == channelName) {
                                subscriptionInfo[usrId][channelId].topics = topics;
                            }
                        });
                    });
                    console.log('***** Sub info: ', subscriptionInfo);
                    await db.updateSubscriptionInfo(userInfo.accountId, userInfo.userId, subscriptionInfo);
                } else {
                    console.log('No subscription info found');
                }
            }
        } catch (error) {
            console.log('Error: ', error);
        }
    }

    async deleteUserSubscription(accountId, userId, channelName) {
        console.log('deleteUserSubscription start');
        const db = new DynamoDBUtil();
        let found = false;
        try {
            const isZoomAdmin = await this.isUserZoomAdmin(accountId, userId);
            const validChannels = await this.getValidSubscriptionChannelList(accountId, userId);
            const isOnetoOne = channelName.indexOf('@') >= 0;
            let othersChannel = false;
            if ((validChannels && !utils.isEmpty(validChannels)) || isOnetoOne) {
                let channelId = utils.getKeyByValue(validChannels, channelName);
                if (!channelId && isZoomAdmin) {
                    othersChannel = true;
                }
                if (isOnetoOne && !othersChannel) {
                    channelId = `${userId.toLowerCase()}@xmpp.zoom.us`;
                }
                if (channelId && !othersChannel) {
                    const userInfo = await db.getSubscriptionInfo(accountId);
                    if (userInfo && userInfo.subscriptionInfo) {
                        const { subscriptionInfo } = userInfo;
                        if (subscriptionInfo[userId] && subscriptionInfo[userId][channelId]) {
                            delete subscriptionInfo[userId][channelId];
                            found = true;
                        }
                        console.log('***** Sub info: ', subscriptionInfo);
                        await db.updateSubscriptionInfo(userInfo.accountId, userInfo.userId, subscriptionInfo);
                    } else {
                        console.log('No subscription info found');
                    }
                } else {
                    console.log(`Channel ${channelName} is not a valid channel!`);
                }
            }
            if (isZoomAdmin && !found) {
                const userInfo = await db.getSubscriptionInfo(accountId);
                if (userInfo && userInfo.subscriptionInfo) {
                    const { subscriptionInfo } = userInfo;
                    Object.keys(subscriptionInfo).forEach((uId) => {
                        Object.keys(subscriptionInfo[uId]).forEach((channelId) => {
                            const channel = subscriptionInfo[uId][channelId];
                            if (channel.channelName == channelName) {
                                delete subscriptionInfo[uId][channelId];
                            }
                        });
                    });
                    console.log('***** Sub info: ', subscriptionInfo);
                    await db.updateSubscriptionInfo(userInfo.accountId, userInfo.userId, subscriptionInfo);
                } else {
                    console.log('No subscription info found');
                }
            }
        } catch (error) {
            console.log('Error: ', error);
        }
    }

    async getSubscriptionInfo(accountId, userId) {
        console.log('getSubscriptionInfo - Start ', userId);
        const subscriptionMap = {};
        try {
            const db = new DynamoDBUtil();
            const validChannels = await this.getValidSubscriptionChannelList(accountId, userId);
            const isZoomAdmin = await this.isUserZoomAdmin(accountId, userId);
            const userInfo = await db.getSubscriptionInfo(accountId);
            if (userInfo && userInfo.subscriptionInfo && !utils.isEmpty(userInfo.subscriptionInfo) && validChannels && !utils.isEmpty(validChannels)) {
                const subscriptions = userInfo.subscriptionInfo[userId];
                console.log('Subs: ', subscriptions);
                if (subscriptions && !utils.isEmpty(subscriptions)) {
                    Object.keys(validChannels).forEach((channelId, index) => {
                        console.log(`ChannelID: ${channelId} subs: ${subscriptions[channelId]}`);
                        if (subscriptions[channelId]) {
                            const { topics } = subscriptions[channelId];
                            const { channelName } = subscriptions[channelId];
                            if (topics && topics.length > 0) {
                                subscriptionMap[channelName] = topics;
                            }
                        }
                    });
                    console.log('Subscription map: ', subscriptionMap);
                }
            } else {
                console.log('No channels: ', validChannels, userInfo.subscriptionInfo);
            }
            if (isZoomAdmin) {
                console.log('------------------- Zoom admin');
                if (userInfo && userInfo.subscriptionInfo && !utils.isEmpty(userInfo.subscriptionInfo)) {
                    Object.keys(userInfo.subscriptionInfo).forEach((urId) => {
                        Object.keys(userInfo.subscriptionInfo[urId]).forEach((channelId) => {
                            const channel = userInfo.subscriptionInfo[urId][channelId];
                            const { channelName } = channel;
                            const { topics } = channel;
                            subscriptionMap[channelName] = topics;
                        });
                    });
                }
            } else {
                console.log('------------------- Non Zoom admin');
                if (userInfo && userInfo.subscriptionInfo && !utils.isEmpty(userInfo.subscriptionInfo)) {
                    if (userInfo.subscriptionInfo[userId]) {
                        Object.keys(userInfo.subscriptionInfo[userId]).forEach((channelId) => {
                            const channel = userInfo.subscriptionInfo[userId][channelId];
                            const { channelName } = channel;
                            const { topics } = channel;
                            if (channelName.indexOf('@') >= 0) {
                                subscriptionMap[channelName] = topics;
                            }
                        });
                    }
                }
            }
        } catch (error) {
            console.log('Error: ', error);
        }
        console.log('Subscriptions: ', subscriptionMap);
        return subscriptionMap;
    }

    async getAllChannels(accountId, userId) {
        console.log('getAllChannels - Start ', userId);
        let channels = [];
        try {
            const isZoomAdmin = await this.isUserZoomAdmin(accountId, userId);
            const channelList = await this.getValidSubscriptionChannelList(accountId, userId);
            if (channelList && !utils.isEmpty(channelList)) {
                channels = utils.getValues(channelList);
                console.log('Channel list *******************: ', channels);
            }
            if (isZoomAdmin) {
                const db = new DynamoDBUtil();
                const userInfo = await db.getSubscriptionInfo(accountId);
                if (userInfo && userInfo.subscriptionInfo && !utils.isEmpty(userInfo.subscriptionInfo)) {
                    Object.keys(userInfo.subscriptionInfo).forEach((userKey) => {
                        Object.keys(userInfo.subscriptionInfo[userKey]).forEach((channelId) => {
                            const channel = userInfo.subscriptionInfo[userKey][channelId];
                            const { channelName } = channel;
                            if (!channels.includes(channelName)) {
                                channels.push(channelName);
                            }
                        });
                    });
                }
            } else {
                const db = new DynamoDBUtil();
                const userInfo = await db.getSubscriptionInfo(accountId);
                if (userInfo && userInfo.subscriptionInfo && !utils.isEmpty(userInfo.subscriptionInfo)) {
                    if (userInfo.subscriptionInfo[userId]) {
                        Object.keys(userInfo.subscriptionInfo[userId]).forEach((channelId) => {
                            const channel = userInfo.subscriptionInfo[userId][channelId];
                            const { channelName } = channel;
                            if ((channelName.indexOf('@') >= 0) && (!channels.includes(channelName))) {
                                channels.push(channelName);
                            }
                        });
                    }
                }
            }
        } catch (error) {
            console.log('Error: ', error);
        }
        console.log('No channels found');
        return channels.length > 0 ? channels : null;
    }

    getChannelListAPI(userId, token) {
        const options = {
            method: 'GET',
            url: `https://api.zoom.us/v2/im/users/${userId}/channels`,
            headers: {
                authorization: `Bearer ${token}`
            }
        };

        return new Promise((resolve, reject) => {
            request(options, (error, response, body) => {
                if (error) reject(error);

                resolve(body);
            });
        });
    }

    async getConnectionTokens(accountId) {
        console.log('Get connection tokens - Start');
        const db = new DynamoDBUtil();
        try {
            const tokens = await db.getZoomAccessToken(accountId);
            if (tokens && tokens.zoomConnectionTokens) {
                const connectionTokens = tokens.zoomConnectionTokens;
                const connection = await oauth2Client.connectByRefresh(connectionTokens.refresh_token);
                await db.updateZoomConnectionTokens(accountId, connection.tokens);
                console.log('Updated connection tokens successfully');
                return connection.tokens;
            }
        } catch (error) {
            console.log('Error: ', error);
        }
        console.log('No connction tokens fetched');
        return null;
    }

    async getChannelList(accountId, userId) {
        try {
            const tokens = await this.getConnectionTokens(accountId);
            if (tokens && tokens.access_token) {
                const channelList = await this.getChannelListAPI(userId, tokens.access_token);
                if (channelList) {
                    console.log('Channel list: ', channelList);
                    return channelList;
                }
            } else {
                console.log('Connection tokens not found!');
            }
        } catch (error) {
            console.log('Error occured in getChannelList: ', error);
        }
        return null;
    }

    async getValidSubscriptionChannelList(accountId, userId) {
        const finalChannels = {};

        try {
            const db = new DynamoDBUtil();
            let channelListJson = await this.getChannelList(accountId, userId);
            if (channelListJson && channelListJson.length > 0) {
                channelListJson = JSON.parse(channelListJson);
                const channelMap = {};
                const channelList = [];
                channelListJson.map((channel) => {
                    const jId = channel.group_jid;
                    const { name } = channel;
                    channelList.push(jId);
                    channelMap[jId] = name;
                    return jId;
                });

                let lockedChannels = [];
                if (channelList) {
                    const userInfo = await db.getSubscriptionInfo(accountId);
                    if (userInfo && userInfo.subscriptionInfo && !utils.isEmpty(userInfo.subscriptionInfo)) {
                        const { subscriptionInfo } = userInfo;
                        Object.keys(subscriptionInfo).forEach((uId, index) => {
                            const subscriptions = subscriptionInfo[uId];
                            console.log('Subsc: ', subscriptions);
                            if (uId != userId) {
                                lockedChannels.push(...Object.keys(subscriptions));
                            }
                        });
                    }
                    lockedChannels = [...new Set(lockedChannels)];
                    console.log('Locked channels: ', lockedChannels);
                    if (lockedChannels && channelList.length > 0) {
                        const channels = channelList.filter(x => !lockedChannels.includes(x));
                        channels.map((channel) => {
                            if (channel in channelMap) {
                                finalChannels[channel] = channelMap[channel];
                            }
                            return channel;
                        });
                    }
                }
            } else {
                console.log('User not subscribed to any channels');
            }
        } catch (error) {
            console.log(error);
        }
        console.log(`Valid channels for the user ${userId}:`);
        console.log(finalChannels);
        return finalChannels;
    }

    async getConnectUrl(accountId, userId) {
        console.log('getConnectUrl - Start ', userId);
        try {
            let redirectUri = config.bm_redirect_uri;
            redirectUri = encodeURIComponent(redirectUri);
            console.log('Encoded redirect URL: ', redirectUri);

            const url = `${config.bm_oauth_url}?client_id=${config.bm_client_id}&scope=read&response_type=code&redirect_uri=${redirectUri}`;
            console.log('URL: ', url);
            return url;
        } catch (error) {
            console.log('Error: ', error);
        }
        return null;
    }

    getAccountMappingDetailsForOAuth(userAccountEncoded) {
        console.log('getAccountMappingDetailsForOAuth - Start');
        try {
            const cipherAccountIdUserId = utils.decode(userAccountEncoded);
            const cipherAccountId = cipherAccountIdUserId.split('_')[0];
            const cipherUserId = cipherAccountIdUserId.split('_')[1];
            const accountId = utils.decrypt(cipherAccountId);
            const userId = utils.decrypt(cipherUserId);
            console.log(`AccountId: ${accountId}, UserId: ${userId}`);
            return [accountId, userId];
        } catch (error) {
            console.log('Error: ', error);
        }
        return null;
    }

    getZoomUserInfo(accessToken, userId) {
        const options = {
            method: 'GET',
            url: `https://zoom.us/v2/users/${userId}`,
            headers: {
                Authorization: `Bearer ${accessToken}`,
                'Content-Type':
                    'application/json'
            }
        };
        return new Promise((resolve, reject) => {
            request(options, (error, response, body) => {
                if (error) reject(error);
                else resolve(JSON.parse(body));
            });
        });
    }

    async updateUserOneToOneChannelInfo(accountId, userId, channelId) {
        console.log('Update User One to One Chanel Info - Start');
        const db = new DynamoDBUtil();
        try {
            if (channelId.indexOf(userId.toLowerCase()) >= 0) {
                const userInfo = await db.getSubscriptionInfo(accountId);
                if (userInfo) {
                    if (userInfo.subscriptionInfo) {
                        const { subscriptionInfo } = userInfo;
                        if (!subscriptionInfo[userId] || !subscriptionInfo[userId][channelId]) {
                            try {
                                const tokens = await this.getConnectionTokens(accountId);
                                if (tokens && tokens.access_token) {
                                    const userData = await this.getZoomUserInfo(tokens.access_token, userId);
                                    console.log(userData);
                                    if (userData) {
                                        let name = '';
                                        if (userData.first_name) {
                                            name = userData.first_name;
                                        }
                                        if (userData.last_name) {
                                            name = `${name} ${userData.last_name}`;
                                        }

                                        name = `@${name}`;

                                        if (!subscriptionInfo[userId]) {
                                            subscriptionInfo[userId] = {};
                                        }
                                        if (!subscriptionInfo[userId][channelId]) {
                                            subscriptionInfo[userId][channelId] = {};
                                        }

                                        subscriptionInfo[userId][channelId].channelName = name;
                                        subscriptionInfo[userId][channelId].topics = [];

                                        await db.updateSubscriptionInfo(userInfo.accountId, userInfo.userId, subscriptionInfo);
                                    }
                                }
                            } catch (err) {
                                console.log(err);
                            }
                        }
                    }
                }
            }
        } catch (error) {
            console.log('Error: ', error);
        }
    }
}
module.exports = CommonService;
