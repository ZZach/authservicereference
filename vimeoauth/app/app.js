const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const cookieParser = require('cookie-parser');
const app = express();
const hbs = require('hbs');

hbs.registerHelper('ifEquals', function (pre, next, options) {
    return (pre == next) ? options.fn(this) : options.inverse(this);
});

app.set('view engine', 'hbs');
app.use(bodyParser.json());
app.use(cookieParser());
hbs.registerPartials(`${__dirname}/views`);
app.use(compression());


module.exports = app;
