import React from 'react';
import { hot } from 'react-hot-loader';
// import { HashRouter as Router, Route, Link } from 'react-router-dom';
import './app.scss';
import queryString from 'query-string';
import Home from './Home';
import AuthSuccess from './AuthSuccess';
import AuthFail from './AuthFail';
import ZoomAuthSuccess from './ZoomAuthSuccess';
import ZoomAuthFail from './ZoomAuthFail';
import DeauthSuccess from './DeauthSuccess';
import DeauthFail from './DeauthFail';
import NotifConfig from './NotifConfig';
import OauthInfo from './OauthInfo';
// import ScopeLoading from '@components/ScopeLoading';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.path = window.location.pathname;
        this.page = 'home';
        console.log('props', props);
        if (window.location.search && window.location.search.length > 0) {
            // console.log('WIndow location search: ', window.location.search);
            const q = queryString.parse(window.location.search);
            // console.log(q.p);
            if (q.p) {
                this.page = q.p;
            }
            if (q.state) {
                this.state = q.state;
            }
            // if (q.accountId && q.userId) {
            //     this.accountId = q.accountId;
            //     this.userId = q.userId;
            // }
            // console.log(this.page);
        }
    }

    render() {
        if (this.page == 'home') {
            return (
                <React.Fragment><Home></Home></React.Fragment>
            );
        } if (this.page == 'authSuccess') {
            return (
                <React.Fragment><AuthSuccess></AuthSuccess></React.Fragment>
            );
        } if (this.page == 'authFail') {
            return (
                <React.Fragment><AuthFail></AuthFail></React.Fragment>
            );
        } if (this.page == 'zoomAuthFail') {
            return (
                <React.Fragment><ZoomAuthFail></ZoomAuthFail></React.Fragment>
            );
        } if (this.page == 'zoomAuthSuccess') {
            return (
                <React.Fragment><ZoomAuthSuccess></ZoomAuthSuccess></React.Fragment>
            );
        }  if (this.page == 'notifConfig') {
            return (
                <React.Fragment><NotifConfig {...this}></NotifConfig></React.Fragment>
            );
        } if (this.page == 'deauthSuccess') {
            return (
                <React.Fragment><DeauthSuccess></DeauthSuccess></React.Fragment>
            );
        } if (this.page == 'deauthFail') {
            return (
                <React.Fragment><DeauthFail></DeauthFail></React.Fragment>
            );
        } if (this.page == 'oauthInfo') {
            return (
                <React.Fragment><OauthInfo></OauthInfo></React.Fragment>
            );
        }
        return (
            <React.Fragment><Home></Home></React.Fragment>
        // <React.Fragment><NotifConfig></NotifConfig></React.Fragment>
        );

        // return <React.Fragment><Home></Home><ScopeLoading /></React.Fragment>;
    }
}

let NewApp = App;

if (process.env.NODE_ENV === 'development' && process.env.HOT === true) {
    NewApp = hot(module)(App);
}

export default NewApp;
