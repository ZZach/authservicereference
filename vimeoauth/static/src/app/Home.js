import React from 'react';
import './home.scss';
import logo from '../assets/images/ZoomLogo.png';
import rcClass from 'rc-class';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import { List } from 'semantic-ui-react';
import "./AppKey.css";
import Service from "./Services";
import queryString from 'query-string';

let cn = rcClass('article');

class Home extends React.Component {
	render() {
		return (
			<div {...cn()}>
				<title>Page Not Found</title>
				<div {...cn('header')}>
					<div {...cn('header-content')}>
						<a href='https://zoom.us/' title='go to zoom site'>
							<img
								{...cn(null, 'zoom-logo')}
								alt='zoom'
								src={logo}
								style={{ height: '35px' }}
							/>
						</a>
					</div>
				</div>
				<div {...cn('config-content')} style={{ paddingLeft: '40%'}}>
				<h1 style={{ fontWeight: 'bold' }}>404 Error</h1>
				<h4 style={{ fontWeight: 'bold' }}>Oh Snap! Double Check Your URL</h4>
				</div>
			</div>
		);
	}
}

export default Home;
