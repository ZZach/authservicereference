import React from 'react';
import './home.scss';
import rcClass from 'rc-class';
import logo from '../assets/images/ZoomLogo.png';
import './AppKey.css';

const cn = rcClass('article');
class AuthFail extends React.Component {
    render() {
        return (
            <div {...cn()}>
                <title>Auth Fail</title>
                <div {...cn('header')}>
                    <div {...cn('header-content')}>
                        <a href='https://zoom.us/' title='go to zoom site'>
                            <img
                                {...cn(null, 'zoom-logo')}
                                alt='zoom'
                                src={logo}
                                style={{ height: '35px' }}
                            />
                        </a>
                    </div>
                </div>
                <div {...cn('config-content')} style={{ paddingLeft: '22%' }}>
                    <h1 style={{ fontWeight: 'bold' }}>Not Able to Link Zoom With Vimeo. Try Again.</h1>
                </div>
            </div>
        );
    }
}
export default AuthFail;
