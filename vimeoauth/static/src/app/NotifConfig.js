import React from 'react';
import './home.scss';
import { message } from 'antd';
import ConfigurePage from '@components/ManageSubscription';
import utils from '@utils';
import rcClass from 'rc-class';
import BlockUi from 'react-block-ui';
import logo from '../assets/images/ZoomLogo.png';
import './ui-blocker.css';
import SubscriptionServices from './subscription-services';
import CryptoJS from 'crypto-js';


const cn = rcClass('article');

function removeTopicFromList(topic, topicList) {
    for (let i = topicList.length - 1; i >= 0; i--) {
        if (topicList[i] === topic) {
            topicList.splice(i, 1);
        }
    }
    return topicList;
}

class NotifConfig extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subs: {},
            allTopics: [],
            selectedTopics: [],
            allChannels: [],
            selectedChannel: '',
            blocking: true,
            mixId: this.props.state
        };
    }

  testPageRequest = () => {
      utils.request(`/${process.env.app}/testPageRequest`)
          .then((da) => {
              if (typeof da === 'object') { da = JSON.stringify(da); }
              message.success(da);
          })
          .catch((error) => {
              if (typeof error === 'object') { error = JSON.stringify(error); }
              message.error(error);
          });
  }

  async componentDidMount() {
      console.log('Did mount');
      try {
          const services = new SubscriptionServices();
          this.setState({
              subs: await services.getSubscribedChannels(this.state.mixId),
              allTopics: await services.getAllTopics(this.state.mixId),
              allChannels: await services.getChannels(this.state.mixId),
              blocking: false
          });
      } catch (e) {
          this.setState({
              blocking: false
          });
      }
      console.log(this.state.subs);
  }

  handleChannelSelect = channel => (event) => {
      this.setState({
          selectedChannel: event.target.value
      });
  };


  handleCheck = (e, topic) => {
      console.log(e.target.checked, topic);
      if (e.target.checked) {
          this.setState({
              selectedTopics: [...this.state.selectedTopics, topic]
          });
      } else {
          this.setState({
              selectedTopics: removeTopicFromList(topic, this.state.selectedTopics)
          });
      }
      console.log(this.state.selectedTopics);
  };

  editComponent = (channel) => {
      this.setState({
          selectedChannel: channel,
          selectedTopics: this.state.subs[channel]
      });
  }

  deleteComponent = (channel) => {
      console.log('In delete component', channel);
      this.deleteSubscription(channel);
  }

  deleteSubscription = async (channel) => {
      try {
          console.log(this.state.selectedTopics);
          console.log(this.state.selectedChannel);
          this.setState({
              blocking: true
          });
          const services = new SubscriptionServices();
          let token = await services.getcsrf();
          token=JSON.parse(token);
          console.log('get csrf', token);
          console.log('token.token', token.token);
          await services.deleteSubscription(channel, token.token);
      } catch (error) {
          console.log(error);
      }
      const services = new SubscriptionServices();
      this.setState({
          selectedChannel: '',
          selectedTopics: [],
          subs: await services.getSubscribedChannels(this.state.mixId),
          allChannels: await services.getChannels(this.state.mixId),
          blocking: false
      });
  }

  saveSubscription = async () => {
      try {
          console.log(this.state.selectedTopics);
          console.log(this.state.selectedChannel);
          this.setState({
              blocking: true
          });
          const services = new SubscriptionServices();
          let token = await services.getcsrf();
          token=JSON.parse(token);
          console.log('get csrf', token);
          console.log('token.token', token.token);
          await services.saveSubscription(this.state.selectedChannel, this.state.selectedTopics, token.token);
      } catch (error) {
          console.log(error);
      }
      const services = new SubscriptionServices();
      this.setState({
          selectedChannel: '',
          selectedTopics: [],
          subs: await services.getSubscribedChannels(this.state.mixId),
          blocking: false
      });
  }

  render() {
      console.log('render');
      return (
          <div {...cn()}>
              <div {...cn('header')}>
                  <div {...cn('header-content')}>
                      <a href='https://zoom.us/' title='go to zoom site'>
                          <img
                              {...cn(null, 'zoom-logo')}
                              alt='zoom'
                              src={logo}
                          />
                      </a>
                  </div>
              </div>
              <div {...cn('content')}>
                  <BlockUi tag="div" blocking={this.state.blocking}>
                      <ConfigurePage {...this} />
                  </BlockUi>
              </div>
          </div>
      );
  }
}


export default NotifConfig;
