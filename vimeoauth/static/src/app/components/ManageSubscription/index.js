import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import SubscriptionServices from './../../subscription-services';


const useStyles = makeStyles(theme => ({
    root: {
        width: '80%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
        minHeight: '10%',
        height: '199px',
        paddingLeft: '20%'
    },
    table: {
        minWidth: 550,
    },
    formControl: {
        margin: theme.spacing(2),
        width: 280,
        marginLeft: '35%',
        marginBottom: 50
      },
    form: {
        width: '70%',
        paddingTop: '30px',
        textAlign: 'center',
        paddingBottom: '30px',
    },
    cbox: {
        marginLeft: '5px'
    },
    flabel: {
        width: '350px',
        textAlign: 'center',
    },
    deleteBtn: {
        // height: 20
    },
    editBtn: {
        // height: 20
    },
    saveBtn: {
        margin: theme.spacing(3),
    },
}));

const StyledTableCell = withStyles(theme => ({
    /*head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },*/
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
        // position: "sticky",
        // top: 0
    },
    root: { borderBottom: 1, borderColor: 'black', borderStyle: 'solid' }, // or borderTop: '1px solid red'
    body: {
        fontSize: 14,
        fontWeight: 10,
        height: 20
    }
}))(TableCell);



const CHANNELS = ['indecomm', 'zoom', 'automationng'];


export default function ConfigurePageFunc(parent) {
    const services = new SubscriptionServices();
    const classes = useStyles();
    const allTopics = parent.state.allTopics;
    let userSubscribedChannelTopics = parent.state.subs;
    const userChannels = parent.state.allChannels;
      
    
      const topics = (
        <>
            {
                (parent.state.allTopics).map( (topic, index) => (
                    <FormControlLabel
                    className={classes.flabel}
                    key={"topic"+index}
                    value={topic}
                    control={<Checkbox color="primary" onChange={e => parent.handleCheck(e, topic) }
                    id={topic}
                    checked={parent.state.selectedTopics.includes(topic)}
                    className={classes.cbox}  />}
                    label={topic}
                    labelPlacement="start"
                    />  
                ))
            }
        </>
    );

    const channels =  (
        <>
            <InputLabel htmlFor="channel-select" className={classes.channelLabel}>Select Channel</InputLabel>
            <Select
          native
          onChange={parent.handleChannelSelect('channel')}
          value={parent.state.selectedChannel}
          inputProps={{
            name: 'channel',
            id: 'channel-select',
          }}
        >
            <option value=""/>
            {
                userChannels.map( (channel, index) => (
                    <option value={channel} key={"channel"+index}>{channel}</option>
                ))
            }
        </Select>
        </>
    );

    const deleteIcon = channel => (
        <IconButton onClick={() => parent.deleteComponent(channel)} className={classes.deleteBtn}>
          <DeleteIcon color="secondary" />
        </IconButton>
      );
  
      const editIcon = channel => (
        <IconButton onClick={() => parent.editComponent(channel)} className={classes.editBtn}>
          <EditIcon color="primary" />
        </IconButton>
      );

    return (
        <div>
            <h6>Existing Subscriptions</h6>
            <Paper className={classes.root} elevation={0}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <StyledTableCell>Channel Name</StyledTableCell>
                            <StyledTableCell align="center" colSpan={2}>Action</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {Object.keys(userSubscribedChannelTopics).map(channel => (
                            <TableRow key={channel}>
                                <TableCell component="th" scope="row">
                                    {channel}
                                </TableCell>
                                <TableCell align="right">{editIcon(channel)}</TableCell>
                                <TableCell align="right">{deleteIcon(channel)}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
            <FormControl className={classes.form}>
                <h6>Subscriptions</h6>
                <FormGroup aria-label="position" name="position" row>
                    {topics}
                </FormGroup>
                <FormControl className={classes.formControl}>
                    {channels}
                </FormControl>
                <Button variant="contained" size="medium" 
                style={{ backgroundColor: '#2d8dfe', color: 'white', width: '85%', marginLeft: '8%' }}
                disabled={!(parent.state.selectedChannel && parent.state.selectedChannel.length > 0 && parent.state.selectedTopics.length > 0 )}
                className={classes.saveBtn} onClick={parent.saveSubscription}>
                    Save Subscription
                </Button>
            </FormControl>
        </div>
    );
}
