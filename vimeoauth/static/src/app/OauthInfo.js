import React from 'react';
import './home.scss';
import rcClass from 'rc-class';
import logo from '../assets/images/ZoomLogo.png';
import "./AppKey.css";
import Button from '@material-ui/core/Button';
const request = require('request');
let cn = rcClass('article');
class OauthInfo extends React.Component {
  GetOauthPage(){
    console.log('Saving subscriptions');
    return new Promise((resolve, reject) => {
        request('https://udnkid8j5e.execute-api.us-east-1.amazonaws.com/dev/authservice/getoauthurl?appname=vimeo', (error, res, body) => {
            if(error){
                reject(error);
            }
            console.log(body);
            resolve(body);
        });
    })
  }
  StartOauth = async () => {
    try {
        let urlObj = await this.GetOauthPage();
        urlObj = JSON.parse(urlObj);
        console.log(urlObj);
        console.log(urlObj.url);
        window.location.href=urlObj.url;
    } catch (error) {
        console.log(error);
    }
  }
	render() {
		return (
			<div {...cn()}>
				<title>Oauth Info</title>
				<div {...cn('header')}>
					<div {...cn('header-content')}>
						<a href='https://zoom.us/' title='go to zoom site'>
							<img
								{...cn(null, 'zoom-logo')}
								alt='zoom'
								src={logo}
								style={{ height: '35px' }}
							/>
						</a>
					</div>
				</div>
				<div {...cn('config-content')} style={{ paddingLeft: '25%'}}>
				<h1 style={{ fontWeight: 'bold' }}>Information about this app</h1>
        <Button variant="contained" size="medium" 
                style={{ backgroundColor: '#2d8dfe', color: 'white', width: '85%', marginLeft: '8%' }}
                onClick={this.StartOauth}>
                    Click To Oauth
        </Button>
				</div>
			</div>
		);
	}
}	
export default OauthInfo;