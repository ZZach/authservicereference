import React from 'react';
import './home.scss';
import rcClass from 'rc-class';
import logo from '../assets/images/ZoomLogo.png';
import "./AppKey.css";
let cn = rcClass('article');
class DeauthSuccess extends React.Component {
	render() {
		return (
			<div {...cn()}>
				<title>Auth Success</title>
				<div {...cn('header')}>
					<div {...cn('header-content')}>
						<a href='https://zoom.us/' title='go to zoom site'>
							<img
								{...cn(null, 'zoom-logo')}
								alt='zoom'
								src={logo}
								style={{ height: '35px' }}
							/>
						</a>
					</div>
				</div>
				<div {...cn('config-content')} style={{ paddingLeft: '25%'}}>
				<h1 style={{ fontWeight: 'bold' }}>Zoom Deauthorization Success. Vimeo Uninstalled.</h1>
				</div>
			</div>
		);
	}
}	
export default DeauthSuccess;
