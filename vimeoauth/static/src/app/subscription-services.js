const request = require('request');

class SubscriptionServices {

    constructor(){
        this.ENDPOINT_URL = this.getBaseUrl();
    }

    getSubscribedChannels(mixId){
        return new Promise((resolve, reject) => {
            let data = {};
            let options = {
                url: this.ENDPOINT_URL+"/subscribedChannels",
                method: "POST",
                json: data
            };

            request(options, (error, res, body) => {
                if(error){
                    reject(error);
                }
                console.log(body);
                resolve(body);
            });
        })    
    }
    
    getAllTopics(mixId){
        return new Promise((resolve, reject) => {
            let data = {
            }; 
            let options = {
                url: this.ENDPOINT_URL+"/allTopics",
                method: "POST",
                json: data
            };

            request(options, (error, res, body) => {
                if(error){
                    reject(error);
                }
                console.log(body);
                resolve(body);
            });
        })
    }

    getChannels(mixId){
        return new Promise((resolve, reject) => {
            let data = {
            }; 
            let options = {
                url: this.ENDPOINT_URL+"/allChannels",
                method: "POST",
                json: data
            };

            request(options, (error, res, body) => {
                if(error){
                    reject(error);
                }
                console.log(body);
                resolve(body);
            });
        })
    }

    deleteSubscription(channel, csrf) {
        console.log('Saving subscriptions');
        return new Promise((resolve, reject) => {
            let data = {
                channel: channel,
                csrf: csrf
            }; 
            
            let options = {
                url: this.ENDPOINT_URL+"/deleteSubscription",
                method: "POST",
                json: data
            };

            console.log(options);
            request(options, (error, res, body) => {
                if(error){
                    reject(error);
                }
                console.log(body);
                resolve(body);
            });
        })
    }

    saveSubscription(channel, topics, csrf){
        console.log('Saving subscriptions');
        console.log('savesubscriptioncsrf:', csrf);
        return new Promise((resolve, reject) => {
            let data = {
                channel: channel,
                topics: topics,
                csrf: csrf
            }; 
            
            let options = {
                url: this.ENDPOINT_URL+"/saveSubscription",
                method: "POST",
                json: data
            };

            console.log(options);
            request(options, (error, res, body) => {
                if(error){
                    reject(error);
                }
                console.log(body);
                resolve(body);
            });
        })
    }

    getcsrf(){
        console.log('getting csrf');
        return new Promise((resolve, reject) => {
            request('https://udnkid8j5e.execute-api.us-east-1.amazonaws.com/dev/authservice/getcsrf?appname=vimeo', (error, res, body) => {
                if(error){
                    reject(error);
                }
                console.log('csrf', body);
                resolve(body);
            });
        })
    }

    getBaseUrl(){
        let location = window.location.href;
        let str = location.split('?')[0];
        if(str){
            str = str.substring(0, str.lastIndexOf("/"));
            console.log('Base URL: ',str);
            return str;
        }
        return '';
    }
};


export default SubscriptionServices;