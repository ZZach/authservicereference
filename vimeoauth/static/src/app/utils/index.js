import axios from 'axios';
import {observer} from '@components/ScopeLoading';
// import path from 'path';


let request=(...props)=>{
    // let newProps=[].concat(props);
    // let firstProp=newProps[0];
    // let url;
    // if(typeof firstProp==='object'){
    //     firstProp=Object.assign({},firstProp);
    //     url=firstProp.url;
    //     if(!path.isAbsolute(url)){
    //         url=path.join(process.env.app,url);
    //     }
    // }

    let ajaxSn = observer.add(null);
    let finishRequest = () => {
        observer.remove(ajaxSn);
    };
    return new Promise((resolve,reject)=>{
        axios(...props)
        .then((da)=>{
            finishRequest();
            if(typeof da==='object'){
                resolve(da.data);
            }
            else{
                resolve(da);
            }
        })
        .catch((error)=>{
            finishRequest();
            let out;
            if(error.response){
                out=error.response.data;
            }
            else{
                out=error;
            }
            reject(out);
        });
    });
};

let utils={
    request
};


export default utils;

