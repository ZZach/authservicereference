var console = {};
console.log = function(){};
const request = require('request');
const crypto = require('crypto');


class Service {
    constructor() {
        this.ENDPOINT_URL = this.getBaseUrl();
    }

    randomValueHex(len) {
        return crypto
            .randomBytes(Math.ceil(len / 2))
            .toString('hex') // convert to hexadecimal format
            .slice(0, len); // return required number of characters
    }

    async checkOrganization(eKey, auId) {
        // console.log(config.check_org_url);
        const form = {
            orgId: eKey,
            mixId: auId
        };
        const options = {
            headers: {
                'Content-Type': 'text/plain'
            },
            url: `${this.ENDPOINT_URL}/checkOrganization`,
            body: JSON.stringify(form)
        };
        return new Promise((resolve, reject) => {
            request.post(options, (err, res, body) => {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                // console.log("lkdsjfslkfjsdlfksjf"+body);
                resolve(body);
            });
        });
    }

    getBaseUrl() {
        const location = window.location.href;
        let str = location.split('?')[0];
        if (str) {
            str = str.substring(0, str.lastIndexOf('/'));
            console.log('Base URL: ', str);
            return str;
        }
        return '';
    }
}

export default Service;
