const nodepath = require('path');
const themes = require('./themes');
const config = require('../app/config');

module.exports = {
    port: 3005,
    entryIndex: './src/main.js',
    buildDir: '../staticBuild',
    appHtml: './src/index.html',
    hot: true,
    buildAssetsDir: 'assets',
    // buildAssetsDir:process.env.NODE_ENV==='production'?'https://s3...':'assets',
    theme: 'antd',
    themeVariables: themes,
    https: false,
    pageEnv: {
        app: process.env.npm_package_name,
        encKey: config.encryption_key
    },
    webpack: {
        alias() {
            return {
                '@components': nodepath.resolve('./src/app/components'),
                '@utils': nodepath.resolve('./src/app/utils')
            };
        }
    }
    // pageEnv:{

    // },
};
