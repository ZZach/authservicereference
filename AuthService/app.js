const SESSION_SECRET = "Used to sign cookies"
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser')
const session = require('express-session');
const {uuid} = require('uuidv4');
const lodash = require('lodash');
const AWS = require('aws-sdk');
const path = require('path');
let DynamoDBStore = require('connect-dynamodb')(session);
//TODO: allow any app to request any app's info?
//TODO: don't create session for server-server
//TODO: rolling timer?
//TODO: what if someone directly visited auth website and cookie is empty?
//TODO: check session expiration
//TODO: check user's cookie is enabled
//TODO: validator middleware, make sure requests have all required params, otherwise error

let baseURL = 'https://udnkid8j5e.execute-api.us-east-1.amazonaws.com/dev/authservice';
let dynamoDBStoreOptions = {
  table: 'nodebots_AuthenticationSession',
  AWSConfigJSON: {
    region: 'us-east-1'
  },
}

//TODO: cookies not signed
//app.use(express.json());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(session({
  genid: (req) => {
    return uuid(); //TODO should i use uuid as genid or default?
  },
  secret: SESSION_SECRET,
  store: new DynamoDBStore(dynamoDBStoreOptions),
  resave: true,
  saveUninitialized: true,
  name: "chatbotsession",
  cookie: {
    secure: false,
    maxAge: 1000 * 60 * 60,
    sameSite: 'lax'
  }
}));
//hbs.registerPartials(__dirname + '/views');
//app.use(express.static(path.join(__dirname, 'build')));
app.set('views',path.join(__dirname,'views'))
app.set('view engine','hbs');
//app.use(compression());


let docClient = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'});

//TODO: remember to add expires in to this
async function WriteDB(id, value)
{
  return new Promise((resolve, reject) => {
    const params = {
        TableName: 'nodebots_AuthenticationSession',
        Item: {
          id: id,
          value: value,
          expires: Math.floor(Date.now() / 1000)+300
        }
    };
    // console.log(params);
    docClient.put(params, (err, data) => {
        if (err) {
            console.log('Unable to read item. Error JSON:', JSON.stringify(err, null, 2));
            reject(`Unable to read item. Error JSON:${JSON.stringify(err, null, 2)}`);
        } else {
            //  console.log('Added tp token successfully:', JSON.stringify(data, null, 2));
            resolve(true);
        }
    });
  });
}

async function readAndDelete(id)
{
  console.log(id);
  return new Promise((resolve, reject) => {
    const params = {
        TableName: 'nodebots_AuthenticationSession',
        Key: {
          id: id
        },
        ReturnValues: 'ALL_OLD'
    };
    // console.log(params);
    docClient.delete(params, (err, data) => {
        if (err) {
            console.log('Unable to read item. Error JSON:', JSON.stringify(err, null, 2));
            reject(`Unable to read item. Error JSON:${JSON.stringify(err, null, 2)}`);
        } else {
            //  console.log('Added tp token successfully:', JSON.stringify(data, null, 2));
            //TODO: check for empty with lodash
            if (lodash.isEmpty(data)) resolve(data);
            else{
              console.log(data.Attributes.value);
              data.Attributes.value.expires = data.Attributes.expires;
              resolve(data.Attributes.value);
            }
        }
    });
  });
}

async function GenerateConfigURL(inputBody)
{
  let randomId = uuid();
  await WriteDB(randomId, inputBody);
  //tempdb[randomId] = inputBody;
  //check to make sure inputBody has all needed fields (appname, information,redirecturl)
  return baseURL+ '/configsetsession?id='+randomId;
}

async function GenerateOauthURL(inputBody)
{
  let randomId = uuid();
  await WriteDB(randomId, inputBody);
  //tempdb[randomId] = inputBody;
  //check to make sure inputBody has all needed fields (appname, information,redirecturl)
  return baseURL+ '/oauthsetsession?id='+randomId;
}

//TODO: make sure to secure private endpoints, this one does not need express-session
app.post('/authservice/GenerateConfigURL', async (req, res)=>{
  let inputBody = {};
  console.log(req.body);

  //TODO: error if body does not have any
  inputBody.appname = req.body.appname;
  inputBody.redirectURL = req.body.redirectURL;
  inputBody.information = req.body.information;

  console.log(inputBody);
  let url = await GenerateConfigURL(inputBody);
  res.json({resultUrl:url});
});

//TODO: make sure to secure private endpoints, this one does not need express-session
app.post('/authservice/GenerateOauthUrl', async (req, res) => {
  let inputBody = {};
  console.log(req.body);

  //TODO: error if body does not have any
  inputBody.appname = req.body.appname;
  inputBody.oauthurl = req.body.oauthurl;
  inputBody.information = req.body.information;
  inputBody.oauthstate = req.body.oauthstate;
  inputBody.redirect_url = req.body.redirect_url;
  console.log(inputBody);
  let url = await GenerateOauthURL(inputBody);
  res.json({resultUrl:url});
});

//token exchange: session for value and csrf
//verification: return if session and csrf matches



//communicates directly with user's browser
app.get('/authservice/configsetsession', async (req, res)=>{
  //
  let randomId = req.query.id;
  let data = await readAndDelete(randomId);
  if (lodash.isEmpty(data) || Math.floor(Date.now() / 1000) > data.expires)
  {
    res.redirect(baseURL+'/expiredError');
    return;
  }
  //check data exists and not expired
  let appname = data.appname;
  if (!req.session[appname]) req.session[appname] = {};
  if (!req.session[appname].information) req.session[appname].information = {};
  req.session[appname].information[data.information.key] = data.information.value;
  if (!req.session[appname].csrf) req.session[appname].csrf = uuid();
  res.redirect(data.redirectURL);

  //TODO: render error page if token is no longer valid
});

//communicates directly with user's browser
app.get('/authservice/oauthsetsession', async (req, res)=>{
  //
  let randomId = req.query.id;
  let data = await readAndDelete(randomId);
  //check data exists and not expired
  console.log('setsession', lodash.isEmpty(data));
  console.log('randomIdData', data);
  if (lodash.isEmpty(data) || Math.floor(Date.now() / 1000) > data.expires)
  {
    res.redirect(baseURL+'/expiredError');
    return;
  }
  let appname = data.appname;
  if (!req.session[appname]) req.session[appname] = {};
  req.session[appname].oauthurl = data.oauthurl;
  req.session[appname].oauthstate = data.oauthstate; //check?
  req.session[appname].redirect_url = data.redirect_url; //url of the bot
  if (!req.session[appname].information) req.session[appname].information = {};
  req.session[appname].information[data.information.key] = data.information.value;
  if (!req.session[appname].csrf) req.session[appname].csrf = uuid();
  res.redirect(req.session[appname].redirect_url);

  //TODO: render error page if token is no longer valid
});

app.get('/authservice/getcsrf', async (req, res)=>{
  req.session[req.query.appname].csrf = uuid();
  res.json({token: req.session[req.query.appname].csrf});
});

app.get('/authservice/startoauth', async (req, res)=>{
  console.log(req.headers.csrf);
  console.log(req.session[req.query.appname].oauthurl);
  if (req.session[req.query.appname].csrf === req.headers.csrf){
    res.redirect(req.session[req.query.appname].oauthurl);
  } else {
    res.json({message: 'error'});
  }
});

//NOTE: race condition if user sets 2 at the same time, very minor
app.get('/authservice/validatecsrf', (req, res) => {
  console.log('body:', req.query);
  console.log('bodycsrf:', req.query.csrf);
  let incomingcsrf = req.query.csrf
  if (req.session[req.query.appname].csrf === incomingcsrf) res.json({validated: true});
  else res.json({validated: false})
});

//DO NOT CALL FROM YOUR FRONT END, WE DONT WANT THIS INFORMATION TO BE EXPOSED IN FRONT END
app.get('/authservice/getsession', async (req, res)=>{
  console.log('getting session');
  console.log('body.appname', req.query.appname);
  let session = req.session;
  let app = req.query.appname;
  console.log('auth data', session[app]);
  res.json(session[app]);

  //TODO: error if session is expired
});

app.get('/authservice/getoauthurl', async (req, res)=>{
  console.log('getting oauthurl');
  res.json({url: req.session[req.query.appname].oauthurl});

  //TODO: error if session is expired
});

app.get('/authservice/validatesessionandstate', async (req, res) => {
  let incomingState = req.query.state;
  if (incomingState === req.session[req.query.appname].oauthstate) res.json({validated: true, session: req.session[req.query.appname]});
  else {
    res.json({validated: false});
    console.log('invalid state query', req.query.state);
    console.log('invalid state db ',req.session[req.query.appname].oauthstate.toString())
  }
});

app.get('/authservice/expiredError', async (req, res) => {
  res.render('expirederror', {});
});


//app.listen(8080, () => console.log(`Example app listening at http://localhost:8080`))
module.exports = app;