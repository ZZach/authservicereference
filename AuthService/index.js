const serverless = require('serverless-http');
let expressApp=require('./app');

const handler = serverless(expressApp);

module.exports.handler = async (event, context) => {
  return await handler(event, context);
};